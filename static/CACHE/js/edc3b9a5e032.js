'use strict';var app=angular.module('clairesApp',['restangular','angular.filter','rzModule','ngDialog','LocalStorageModule','ngRoute','ngSanitize']);app.config(function(RestangularProvider){RestangularProvider.setBaseUrl(window.location.origin+"/api").addResponseInterceptor(function(data,operation,what,url,response,deferred){var extractedData=data;if(operation==="getList"){if(_.has(data,'results')){extractedData=data.results;extractedData.count=data.count;extractedData.next=data.next;extractedData.previous=data.previous;}}
return extractedData;});});app.config(function($routeProvider,$locationProvider){$routeProvider.when('/:vacId',{templateUrl:'vacancy-grid-tpl',controllerAs:'VacDetailCtrl',controller:['$scope','vacDetail',function($scope,vacDetail){this.vac_detail=vacDetail}],resolve:{vacDetail:function($route,VacService){var id=$route.current.params.vacId
return VacService.Hh_detail(id)}}}).otherwise({templateUrl:'vacancy-grid-tpl-default',controllerAs:'VacDetailCtrl'});});app.config(function($interpolateProvider,$locationProvider){$interpolateProvider.startSymbol('{$');$interpolateProvider.endSymbol('$}');$locationProvider.html5Mode(false).hashPrefix('!');});app.run(function($rootScope){$rootScope._=_;})
app.controller('productsGridCtrl',function($scope){});app.controller('pageCtrl',function($scope){this.per_page=$scope.per_page
this.pages=$scope.$parent.result});app.controller('VacCtrl',function(VacService){var v=this
v.vac_list=[]
v.getAllVacancies=function(){VacService.Hh_list().success(function(data,status,headers,conf){v.vac_list=data.items})}});app.controller('AnketCtrl',function(VacService){this.submitted=false
this.submitForm=function(ngdata){var data=ngdata
data["csrfmiddlewaretoken"]=document.getElementsByName("csrfmiddlewaretoken")[0].value
console.log(data)
VacService.post_anket(data)};});app.controller('filterCtrl',function(ProductService,$httpParamSerializer,$filter,$scope,$timeout,ngDialog,anchorSmoothScroll,localStorageClaires){var f=this
f.preloding=false;f.new_params=null
f.q_req=0;f.cart=localStorageClaires.cart
f.slug=null;f.per_page=30;f.selectedCheckboxesProp=[];f.selectedCheckboxesStores=[];f.post_params={}
f.result=[]
f.count=0
f.post_params={}
f.pager={curr:1,pages:0}
f.pages=f.count
f.sort_types=[{name:"По имени",value:'name'},{name:"По цене убыв.",value:'-product_price__price'},{name:"По цене возр.",value:'product_price__price'},{name:"По артикулу",value:'art'},]
f.sorter=f.sort_types[0]
$scope.$on("slideEnded",function(data){f.post_params.min_price=data.targetScope.rzSliderModel
f.post_params.max_price=data.targetScope.rzSliderHigh
return f.refreshFilterData(f)});f.filter_range={min:10,max:1000,options:{floor:10,ceil:5000}}
f.updateSort=function(){f.preloding=true
f.post_params.ordering=f.sorter.value
f.refreshFilterData(f)}
f.setPage=function(page){if(page<=f.pager.pages&&page>=1){f.preloding=true
f.post_params.page=page
f.pager.curr=page
f.refreshFilterData(f)}}
f.getProduct=function(item){ngDialog.open({template:'product-tpl',className:'ngdialog-theme-default',controllerAs:'productCtrl',controller:['$scope','currentItem','goTo',function($scope,currentItem,goTo){this.product=currentItem
this.goTo=goTo}],resolve:{currentItem:function(){return item},goTo:function(){return function(){window.location=item.url}}},width:'50%'});}
f.goTo=function(slug){window.location="/catalog/"+slug}
f.update=function(slug,item,store,page){console.log(f.is_group);f.slug=slug
f.preloding=true
if(store){var idx=f.selectedCheckboxesStores.indexOf(store)
if(idx>-1){f.selectedCheckboxesStores.splice(idx,1);}
else{f.selectedCheckboxesStores.push(store);}
f.post_params.stores=f.selectedCheckboxesStores
delete f.post_params.page}
if(item&&f.selectedCheckboxesProp){var idx=f.selectedCheckboxesProp.indexOf(item)
if(idx>-1){f.selectedCheckboxesProp.splice(idx,1);}
else{f.selectedCheckboxesProp.push(item);}
f.post_params.prop=f.selectedCheckboxesProp
delete f.post_params.page}
if(f.slug){f.post_params.group=f.slug}
f.q_req++;f.refreshFilterData(f)
f.refreshGroupData(f)}
f.refreshFilterData=function(el){return ProductService.updateFilter(f.post_params).then(function(response){el.props=response.props
el.prices=response.prices
el.stores=response.shops
el.count=response.count
el.result=response.items
el.images=response.images}).then(function(el){f.pager.pages=Math.ceil(f.count/f.per_page)
f.preloding=false;})}
f.refreshGroupData=function(el){var request;if(f.slug){request={site_slug:f.slug}}
if(f.is_group){ProductService.getCateg(request).then(function(response){el.group=response})}else{ProductService.getGroup(request).then(function(response){el.group=response})}}
f.is_resetedFilter=function(){return(f.selectedCheckboxesProp.length==0&&f.selectedCheckboxesStores.length==0&&!f.post_params.max_price&&!f.post_params.min_price)}
f.resetFilter=function(){f.selectedCheckboxesProp=[];f.selectedCheckboxesStores=[];f.post_params={}
f.update(f.slug,0,0)}
f.gotoElement=function(eID){anchorSmoothScroll.scrollTo(eID);}});app.controller('ProductViewCtrl',function($scope,ngDialog,localStorageClaires){var p=this
p.cart=localStorageClaires.cart
p.wish=localStorageClaires.wish
p.addItem=function(product){localStorageClaires.cartAdd(product)}
p.removeItem=function(art){localStorageClaires.cartDelete(art)}
p.proccessdWish=function(product){localStorageClaires.wishProccess(product)
p.updateWishBtnText(product.art)}
p.updateWishBtnText=function(art){p.wishTxt=localStorageClaires.is_inWish(art)?"Удалить из Wish List":"Добавить в Wish List"}
p.deleteWish=function(art){localStorageClaires.wishRemove(art)
p.updateWishBtnText(art)
_.remove(p.user_wishList,function(curObj){return curObj.art==art.toString();});}
p.is_wish=function(product){return localStorageClaires.is_inWish(product.art)}
p.getWishes=function(){if(p.wish.length){return localStorageClaires.getWishItems().then(function success(response){p.user_wishList=response;})}}});app.directive('whenScrollEnds',function(){return{restrict:"A",link:function(scope,element,attrs){var visibleHeight=element.height();var threshold=100;element.scroll(function(){var scrollableHeight=element.prop('scrollHeight');var hiddenContentHeight=scrollableHeight-visibleHeight;if(hiddenContentHeight-element.scrollTop()<=threshold){scope.$apply(attrs.whenScrollEnds);}});}};});