app.factory('ProductService',function(Restangular,DataCache,$q){var r={count:0,getGroup:getGroup,updateFilter:updateFilter,meta:get_meta}
return r
function getGroup(request_param){return Restangular.all('groups/').customGET(null,request_param).then(function(response){if(request_param&&response.results.length==1){return response.results[0]}
return response.results});}
function getCateg(request_param){return Restangular.all('category/').customGET(null,request_param).then(function(response){if(request_param&&response.results.length==1){return response.results[0]}
return response.results});}
function updateFilter(request_param){var cache_str=JSON.stringify(request_param),deferred=$q.defer();res=DataCache.get(cache_str);if(!res){return Restangular.all('products/').customGETLIST(null,request_param).then(function(response){return dataProductsHandler(response,cache_str)});}
deferred.resolve(res);return deferred.promise;}
function dataProductsHandler(response,cache_str){var data={props:[],shops:[],prices:[],count:0,items:[]}
data.count=response.count;data.page={next:response.next,prev:response.previous}
data.items=response
_.forEach(response,function(value,key){var obj=response[key],props=obj.prop,stores=obj.stores,price=obj.price
_.forEach(props,function(v,k){if(!_.some(data.props,v)){data.props.push(v);}});_.forEach(stores,function(v,k){var store={"id":v.store_id,"name":v.store_site_name}
if(!_.some(data.shops,store)){data.shops.push(store);}});_.forEach(price,function(v,k){data.prices.push(v.price);});});DataCache.put(cache_str,data);return data;}
function get_meta(){data={};Restangular.all('products/').getList().then(function(response){data.count=response.count;data.next=response.next;});return data;}});app.service('GroupService',function(Restangular){var api=Restangular.all('groups/'),data={};this.get_list=function(){return api.getList().$object;}
api.getList().then(function(response){data.count=response.count;data.next=response.next;});this.get_meta=function(){return data;}});app.service('VacService',function($http,Restangular){this.hh_uri_vaclist='https://api.hh.ru/vacancies?employer_id=2406100&per_page=100'
this.hh_uri_vacdetail='https://api.hh.ru/vacancies/'
this.Hh_list=function(){var promise=$http({method:'GET',url:this.hh_uri_vaclist});promise.success(function(data,status,headers,conf){return data;});return promise;}
this.Hh_detail=function(id){var promise=$http({method:'GET',url:this.hh_uri_vacdetail+id+"?host=hh.ru"});promise.success(function(data,status,headers,conf){return data;});return promise;}
this.post_anket=function(data){var apply_uri="jobsapply/"
$http({method:'POST',url:'/api/jobsapply/',data:data,transformRequest:angular.identity,headers:{'Content-Type':"application/x-www-form-urlencoded; charset=UTF-8",'X-CSRFToken':data["csrfmiddlewaretoken"]}}).success(function(response){console.log(response)});}})
app.service('localStorageClaires',function(localStorageService,Restangular){this.cart=localStorageService.get("cart")||[]
this.wish=_.uniq(localStorageService.get("wish"))||[]
this.res={}
this.cartAdd=function(obj){product={art:obj.art,price:obj.price,q:obj.quantity||1,store:obj.store}
if(!_.find(this.cart,function(obj){return obj.art==product.art})&&product.store){this.cart.push(product);localStorageService.set("cart",this.cart);}}
this.cartDelete=function(art){if(this.cart.map(_.property('art')).indexOf(art)!=-1){_.remove(this.cart,function(curObj){return curObj.art==art;});localStorageService.set("cart",this.cart);}}
this.wishProccess=function(obj){var art=parseInt(obj.art)
if(!this.is_inWish(art)){this.wish.push(art);}else{_.pull(this.wish,art)}
localStorageService.set("wish",this.wish);}
this.wishRemove=function(art){var art=parseInt(art)
if(this.is_inWish(art)){_.pull(this.wish,art)}
localStorageService.set("wish",this.wish);}
this.is_inWish=function(art){if(this.wish.indexOf(parseInt(art))>-1){return true;}else{return false;}}
this.getWishItems=function(){params={art:this.wish}
return Restangular.all('products_search/').customGETLIST(null,params)}})
app.factory('DataCache',function($cacheFactory){return $cacheFactory('dataCache',{});});app.service('anchorSmoothScroll',function(){this.scrollTo=function(eID){var startY=currentYPosition();var stopY=elmYPosition(eID);var distance=stopY>startY?stopY-startY:startY-stopY;if(distance<100){scrollTo(0,stopY);return;}
var speed=Math.round(distance/100);if(speed>=40)speed=40;var step=Math.round(distance/25);var leapY=stopY>startY?startY+step:startY-step;var timer=0;if(stopY>startY){for(var i=startY;i<stopY;i+=step){setTimeout("window.scrollTo(0, "+leapY+")",timer*speed);leapY+=step;if(leapY>stopY)leapY=stopY;timer++;}return;}
for(var i=startY;i>stopY;i-=step){setTimeout("window.scrollTo(0, "+leapY+")",timer*speed);leapY-=step;if(leapY<stopY)leapY=stopY;timer++;}
function currentYPosition(){if(self.pageYOffset)return self.pageYOffset;if(document.documentElement&&document.documentElement.scrollTop)
return document.documentElement.scrollTop;if(document.body.scrollTop)return document.body.scrollTop;return 0;}
function elmYPosition(eID){var elm=document.getElementById(eID);var y=elm.offsetTop;var node=elm;while(node.offsetParent&&node.offsetParent!=document.body){node=node.offsetParent;y+=node.offsetTop;}return y;}};});app.directive('imgPreload',['$rootScope',function($rootScope){return{restrict:'A',scope:{ngSrc:'@'},link:function(scope,element,attrs){element.on('load',function(){element.addClass('in');}).on('error',function(e){console.log("Не загрузилась картинка:"+e)});scope.$watch('ngSrc',function(newVal){element.removeClass('in');});}};}]);