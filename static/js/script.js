
$(document).bind('ajaxSend', function (e, x, s) {
    $("#ajaxloading").show(340);
});

$(document).bind('ajaxComplete', function (e, x, s) {
    $("#ajaxloading").hide(200);
});




function show_main_modal(){
    $.fancybox.open({
        src: '.register_modal', 
        type : 'inline',
        opts: {
        closeTpl : '<button data-fancybox-close class="fancybox-close-small custom_closed_png"></button>',
        slideShow  : false,
        fullScreen : false,
        thumbs     : false        
        }
    });
}


$(document).ready(function(){
	check_online();
    $(".header__navbar-top--left").click(function(){
        $.removeCookie('popup__is__hidden');
        $.removeCookie('is_registered_user');
    })

});

function check_online(){
	if(!navigator.onLine){
		$(".offline").show();
	}
}

setInterval(check_online, 3000);


