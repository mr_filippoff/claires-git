tinymce.init({
  height: 300,
  selector : "textarea",
  language: 'ru',
  theme:"modern",
  plugins: [
    'advlist autolink lists link image charmap print preview anchor',
    'searchreplace visualblocks code fullscreen',
    'insertdatetime media table contextmenu paste code'
  ],
  toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
   content_css: [
    '/collect_static/css/tiny.min.css',
  ],
  file_browser_callback: "djangoFileBrowser"
});