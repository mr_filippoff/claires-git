ymaps.ready(init);


var data = window.lombards;
ballonOpts = {
    balloonContentLayout: 'my#simplestBCLayout',
    balloonPanelMaxMapArea: 0,
    hideIconOnBalloonOpen: false,
    balloonContent:'my#simplestBCLayout',
    iconLayout: 'default#image',
    iconImageHref: '/static/img/Lombard.png',
    iconImageSize: [39, 27],
    balloonAutoPan: true,
    balloonOffset: [10,-35]
}
 

function init () {
    var myMap = new ymaps.Map("map", {
            center: [55.73, 37.54],
            zoom: 10,
            controls: []
        }),
        lombardCollection = new ymaps.GeoObjectCollection({}, ballonOpts),
        href = "";
        if(data.length>1){
            href = "<a class=\"btn btn-default\" href=\"/item/$[properties.id]\">Подробнее</a>";
        }
        BalloonContentLayout = ymaps.templateLayoutFactory.createClass(
            '<div class="baloonLombard">' +
                '<h3>$[properties.name]</h3>' +
                '<p>Адрес: $[properties.address]</p>' +
                '[if properties.phone]<p>Телефон: $[properties.phone]</p>[endif]' +
                '[if properties.worktime]<p>Время работы: $[properties.worktime]</p>[endif]' + href + '</div>', 
            {
            build: function () {
                BalloonContentLayout.superclass.build.call(this);
            }
        });


        ymaps.layout.storage.add('my#simplestBCLayout', BalloonContentLayout);
        $.each(data, function(k, val) {
            var objdata = val.fields,
            prop = {
                "name":objdata.full_name,
                'address':objdata.address,
                'phone':objdata.phone,
                'worktime':objdata.worktime,
                'id':val.pk
            };
            ymaps.geocode(objdata.yandex_address, {results: 1}).then(function (res) {
                coords =  res.geoObjects.get(0).geometry.getCoordinates();

                if(data.length==1){
                    myMap.setCenter(coords, 16);
                    myMap.controls.add('zoomControl');
                    /*
                    route_name = ["Москва, ст.м." + data[0].fields.metro, data[0].fields.yandex_address];
                    ymaps.route([
                            route_name[0],
                            route_name[1]
                        ]).then(function (route) {
                            myMap.geoObjects.add(route);
                            var points = route.getWayPoints(),
                                lastPoint = points.getLength() - 1;
                            points.options.set('preset', 'islands#redStretchyIcon');
                            points.get(0).properties.set('iconContent', route_name[0]);
                            points.get(lastPoint).properties.set('iconContent', route_name[1]);
                    });
                    */
                }

                lombardCollection.add(new ymaps.Placemark(coords, prop, ballonOpts) );
            });
        });
    myMap.geoObjects.add(lombardCollection);
   
}
