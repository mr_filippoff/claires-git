#!/usr/bin/env python
import os
import sys
import django

try:
    from claires.settings import local as settings
except ImportError as ex:
    try:
        from claires.settings import settings as settings
    except ImportError as ex:
        import sys
        sys.stderr.write("Error: %s.)\n" % repr(ex))
        sys.exit(1)

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "claires.settings.local")
    django.setup()
    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)
