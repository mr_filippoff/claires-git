class toValteraRouter(object):
    apps = [ '']

    def db_for_read(self, model, **hints):
        if model._meta.app_label in self.apps:
            return 'valtera'
        return None

    def db_for_write(self, model, **hints):
        if model._meta.app_label in self.apps:
            return 'valtera'
        return None



