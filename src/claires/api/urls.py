from django.conf.urls import patterns, url, include
from claires.views import ClairesView
from .views import *
from rest_framework import routers
from rest_framework.urlpatterns import format_suffix_patterns
from rest_framework_cache.registry import cache_registry
cache_registry.autodiscover()

router = routers.DefaultRouter()
router.register(r'jobsapply', VacanApplyViewSet)
router.register(r'groups', GroupViewSet)
router.register(r'category', CategoryViewSet)
router.register(r'stores', StoresViewSet)
router.register(r'products', ProductViewSet)
router.register(r'products_search', ProductSearchViewSet)
router.register(r'product_stores', ProductStoreViewSet)
router.register(r'props', PropViewSet)
router.register(r'prop_vars', PropVarViewSet)



urlpatterns = [
    url(r'^', include(router.urls, namespace='api'))
]

#urlpatterns = format_suffix_patterns(urlpatterns)