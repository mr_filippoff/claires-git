# -*- coding: utf-8 -*-
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.decorators import *
from rest_framework import permissions
from rest_framework_extensions.cache.mixins import CacheResponseMixin
from rest_framework_extensions.etag.mixins import ETAGMixin
from claires.catalog.models import *
from claires.catalog.serializers import *
from claires.vacancies.serializers import *
from claires.vacancies.models import JobsApply
from rest_framework import viewsets
from rest_framework import mixins
from rest_framework import filters
from rest_framework import generics
from rest_framework import pagination
import django_filters





""" PAGINATION"""
class HugeResultsSetPagination(pagination.PageNumberPagination):
    page_size = 1000

class LargeResultsSetPagination(pagination.PageNumberPagination):
    page_size = 100

class StandardResultsSetPagination(pagination.PageNumberPagination):
    page_size = 50

class ProductsResultsSetPagination(pagination.PageNumberPagination):
    page_size = 30

class SmallResultsSetPagination(pagination.PageNumberPagination):
    page_size = 10



"""FILTERS"""
class ProductSearchFilter(filters.FilterSet):
    art = django_filters.MultipleChoiceFilter(choices = [(x.art, x.name) for x in Product.objects.all() ] )

    class Meta:
        model = Product
        fields = ['art']

class ProductFilter(filters.FilterSet):
    min_price = django_filters.NumberFilter(name="product_price__price", lookup_expr='gte')
    max_price = django_filters.NumberFilter(name="product_price__price", lookup_expr='lte')
    group =  django_filters.MethodFilter(action='filter_group')
    categories =  django_filters.MethodFilter(action='filter_category')
    prop = django_filters.ModelMultipleChoiceFilter(queryset = PropertyVariant.objects.filter(is_show=True), conjoined=False)
    art = django_filters.MultipleChoiceFilter(choices = [(x.art, x.name) for x in Product.objects.filter(is_show=True) ] )

    class Meta:
        model = Product
        fields = ['prop', 'art' ,'group', 'categories', 'stores', 'min_price', 'max_price']


    def filter_category(self, queryset, value):
        if value:
            try:
                g = Category.objects.get(site_slug = value)
                """
                return queryset.filter(
                    categories__tree_id = g.tree_id,
                    categories__lft__gte = g.lft,
                    categories__rght__lte = g.rght)
                """
                return g.products.filter(is_show=True, art__isnull=False, images__isnull=False)
            except:
                pass
        return queryset


    def filter_group(self, queryset, value):
        if value:
            try:
                g = Group.objects.get(site_slug = value)
                return queryset.filter(
                    group__tree_id = g.tree_id,
                    group__lft__gte = g.lft,
                    group__rght__lte = g.rght)
            except:
                pass
        return queryset

class ProductBalanceFilter(filters.FilterSet):
    class Meta:
        model = ProductBalance
        fields = ['product', 'store', 'quantity',]



class GroupFilter(filters.FilterSet):
    class Meta:
        model = Group
        fields = ['id', 'site_slug']

class CategFilter(filters.FilterSet):
    class Meta:
        model = Category
        fields = ['id', 'site_slug']



"""VIEWSETS"""
class GroupViewSet(ETAGMixin, CacheResponseMixin, viewsets.ReadOnlyModelViewSet):
    queryset = Group.objects.filter(is_show=True)
    serializer_class = GroupSerializer
    pagination_class = HugeResultsSetPagination
    filter_class = GroupFilter
    filter_backends = (filters.DjangoFilterBackend,)

    def get_view_description(self, html=True):
        return u"Возвращает разделы каталога"



class CategoryViewSet(CacheResponseMixin, viewsets.ReadOnlyModelViewSet):
    queryset = Category.objects.filter(in_menu=True)
    serializer_class = CategorySerializer
    pagination_class = HugeResultsSetPagination
    filter_class = CategFilter
    filter_backends = (filters.DjangoFilterBackend,)

    def get_view_description(self, html=True):
        return u"Возвращает псевдо категории каталога"


class StoresViewSet(CacheResponseMixin, viewsets.ReadOnlyModelViewSet):
    queryset = Stores.objects.filter(is_show=True).order_by("city","date_created")
    serializer_class = StoresSerializer
    pagination_class = SmallResultsSetPagination

    def get_view_description(self, html=True):
        return u"Возвращает магазины"



class ProductViewSet(ETAGMixin, CacheResponseMixin, viewsets.ReadOnlyModelViewSet):
    queryset = Product.objects.filter(is_show=True, images__isnull=False)
    #queryset = Product.objects.filter(is_show=True)
    serializer_class = ProductSerializer
    filter_backends = (filters.OrderingFilter,filters.DjangoFilterBackend,)
    filter_class = ProductFilter
    pagination_class = ProductsResultsSetPagination
    ordering_fields = ('product_price__price', 'name', 'date_created', 'art','barcode')


class ProductSearchViewSet(CacheResponseMixin, viewsets.ReadOnlyModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    filter_class = ProductSearchFilter
    pagination_class = LargeResultsSetPagination
    filter_backends = (filters.OrderingFilter,filters.DjangoFilterBackend,)
    ordering_fields = ('product_price__price', 'name', 'date_created', 'art','barcode')

class ProductStoreViewSet(ETAGMixin, viewsets.ReadOnlyModelViewSet):
    queryset = ProductBalance.objects.filter(store__is_show=True)
    serializer_class = ProductStoreSerializer
    filter_class = ProductBalanceFilter
    pagination_class = SmallResultsSetPagination


class PropViewSet(ETAGMixin, viewsets.ReadOnlyModelViewSet):
    queryset = Property.objects.filter(is_show=True, value_type=2)
    serializer_class = PropSerializer
    pagination_class = LargeResultsSetPagination

class PropVarViewSet(ETAGMixin, viewsets.ReadOnlyModelViewSet):
    queryset = PropertyVariant.objects.filter(is_show=True)
    serializer_class = PropertyVariantSerializer
    pagination_class = LargeResultsSetPagination


class VacanApplyViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = JobsApply.objects.all()
    serializer_class = JobsApplySerializer


    def list(self, request):
        queryset = self.get_queryset()
        serializer = JobsApplySerializer(queryset, many=True)
        return Response(serializer.data)

    def get_queryset(self):
        if self.request.user.is_staff:
            return JobsApply.objects.all()
        else:
            return []


