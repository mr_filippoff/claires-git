from django.conf.urls import patterns, url, include
from .views import SearchView

urlpatterns = patterns('',
    url(r'$', SearchView.as_view()),
)

