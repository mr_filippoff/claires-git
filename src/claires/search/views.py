# -*- coding: utf-8 -*-
import json
from django.shortcuts import get_object_or_404
from django.views.generic import View
from django.http import Http404, HttpResponse
from django.conf import settings
from django.db.models import Q
from claires.views import ClairesView
from claires.catalog.models import *
from claires.pages.models import *

class SearchView(ClairesView):

	template_name = "search/results.html"

	def get(self, request, *args, **kwargs):
		context = self.get_context_data(request, **kwargs)
		context["result"] = []
		s = request.GET.get("s", None)
		if s:
			if not s.isdigit():
				context["result"].extend( Product.objects.filter(
						Q(site_name__iexact = s) | Q(name__iexact = s) | Q(site_name__istartswith = s) | Q(name__istartswith = s)
					).only("name","site_name", "site_slug", "images")
				)
				context["result"].extend( Page.objects.filter(
						Q(name__icontains = s) | Q(annotate__icontains = s) |  Q(content__icontains = s)
					).only("name","slug","images","section__slug")
				)
			else:
				context["result"].extend( Product.objects.filter(art__startswith = s) )
		response = self.render_to_response(context)
		return response


	def get_context_data(self, request, **kwargs):
		return {}
