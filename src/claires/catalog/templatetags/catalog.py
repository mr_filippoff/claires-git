# -*- coding: utf-8 -*-
from django import template
from django.db.models import Count, Max, Min
from django.shortcuts import get_object_or_404
from django.apps import apps
from claires.catalog.models import *
from claires.utils import *
register = template.Library()

@register.inclusion_tag('catalog/breadcrumbs.html')
def breadcrumb(slug, model): 
    obj = apps.get_model(app_label='catalog', model_name=model)
    try:
        item = get_object_or_404(obj, site_slug=slug)    
        breads = item.breadcrumbs()
        breads.sort(reverse=False)
        return {
            "breadcrumbs":breads,
            "current":item.site_name or item.name
        }
    except:
        return {}



@register.filter
def utm_label(obj, utm_medium="site"):
    utm_campaign = "undefined"
    if hasattr(obj, "name"):
        f = obj.name
    elif hasattr(obj, "site_name"):
        f = obj.site_name
    else:
        f = "homer"
    utm_campaign = transliterate(f)
    return u"utm_campaign={}&utm_source=site&utm_medium={}".format(utm_campaign, utm_medium)

