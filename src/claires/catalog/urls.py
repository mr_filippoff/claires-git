try:
    from django.conf.urls import patterns, url, include
except ImportError:
    from django.conf.urls.defaults import *
from claires.views import ClairesView
from .views import *


urlpatterns = patterns('',
    url(r'^theme/(?P<category_slug>[\w\d\-]+)', CatalogCategoryPseudoView.as_view()),
    url(r'^(?P<group_slug>[\w\d\-]+)/(?P<product_slug>[\w\d\-]+)', CatalogProductView.as_view()),
    url(r'^(?P<group_slug>[\w\d\-]+)', CatalogCategoryView.as_view()),
    url(r'^$', CatalogView.as_view()),
)

