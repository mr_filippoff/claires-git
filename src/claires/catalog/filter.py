# -*- coding: utf-8 -*-
import json
import datetime
from django.shortcuts import get_object_or_404
from django.views.generic import View, TemplateView
from django.http import Http404, HttpResponse
from django.conf import settings
from claires.views import ClairesView
from claires.catalog.models import *
from django.core.cache import cache

class Filter(TemplateView):
    template_name = "catalog/ajax_result.html"

    def get(self, request, *args, **kwargs):
        data = request.GET.copy()
        fullpath = request.get_full_path()
        cat = get_object_or_404(Group, site_slug = kwargs.get("group_slug", None))
        qs_products =  self.get_cache("products_%s"%cat.siteslug, Product.objects.filter(is_show=True, group=cat))
        qs_properties = self.get_cache("properties", Property.objects.filter(is_show=True))
        if data.lists():
            for _tuple  in data.lists():
                qs_products = qs_products.filter(prop__id__in = _tuple[1])
        _props = []
        map(lambda x: _props.extend(map(lambda z: int(z), x[1])), data.lists())
        l_props = []
        if qs_products:
            val_props = set([k for a in qs_products for k in a.prop.values_list("id")])
            l_props = [k[0] for k in val_props]

        context = {
            "l_props":l_props,
            #"filter":self.render_filter(checked = _props, qs = qs, cat =cat),
            "params":_props,
            "properties": qs_properties,
            "count":  qs_properties.count(),
            "fullpath":fullpath,
            "group":cat,
             "groups": Group.objects.filter(is_show=True, parent=None)
        }
        return self.render_to_response(context)


    def get_cache(self, key, qs):
        cached = cache.get(key, None)
        if not cached:
            cached = qs
            cache.set(key, cached, 10)
        return cached


    def _filters(self):
        return Property.objects.filter(is_show=True)

    def render_filter(self, checked = [], qs = None, cat =None):
        res = u"""<div class="filter__item"><h3>{}</h3><ul>""".format(cat)
        if cat:
            childs = cat.childs()
            for c in childs:
                res+= u"""
                    <li>
                        <a href="/catalog/{cat}">{name}</a>
                    </li>""".format(
                        cat=c.siteslug, 
                        name = c.sitename 
                    )
        res+=u"</ul></div>"

        l_props = []
        if qs:
            val_props = set([k for a in qs for k in a.prop.values_list("id")])
            l_props = [k[0] for k in val_props]
        for pobj in self._filters():
            res += u"""<div class="filter__item"><h3>{}</h3><ul>""".format(pobj.sitename)
            for p in  pobj.variants.filter(is_show=True):
                prop_id = str(p.id)
                checked_format = u"checked=\"checked\"" if  prop_id in checked else ""
                if p.id in l_props:
                    res+= u"""
                    <li>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="{val}" name="{val_name}" {cheked}>{name}
                            </label>
                        </div>
                    </li>""".format(
                        val=p.id, 
                        val_name=pobj.siteslug, 
                        cheked=checked_format, 
                        name = p.sitename 
                    )
            res+=u"</ul></div>"
        return res            

    def output_filter(self):
        qs = Product.objects.filter(is_show=True)
        return self.render_filter(qs=qs)