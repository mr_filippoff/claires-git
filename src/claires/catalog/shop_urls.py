try:
    from django.conf.urls import patterns, url, include
except ImportError:
    from django.conf.urls.defaults import *
from .views import *


urlpatterns = patterns('',
    url(r'^$', ShopsView.as_view()),
)

