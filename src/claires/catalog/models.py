# -*- coding: utf-8 -
import datetime
import mptt
import os
import json
from django.db import  models
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericRelation
from django.conf import settings
from claires.images.models import Image
from claires.seo.models import Seo
from claires.seo.conf import *
from claires.locations.models import *
from claires.settings.settings import root

TYPE_IN = (
    (1, u'Строка'),
    (2, u'Справочник'),
)
QS_DEFAULT_VALUE_ARGS = ('site_name','site_slug', 'id', 'name')


#ABSOLUTE_URL_SLUG_FIELD = "id"
ABSOLUTE_URL_SLUG_FIELD = "site_slug"

class Base(models.Model):
    cid = models.CharField(u"Ид",max_length=50)
    name = models.CharField(u"Наименование/значение", max_length=255,null=True, blank=True)
    site_name = models.CharField(u"Имя для сайта", max_length=255, null=True, blank=True)
    site_slug = models.CharField(u"Ссылка для сайта", max_length=255, null=True, blank=True)
    date_created = models.DateTimeField(u"Дата создания", default=datetime.datetime.now)
    is_show =  models.BooleanField(u"Виден на сайте", default=True)


    @property
    def sitename(self):
        return self.site_name or self.name

    @property
    def siteslug(self):
        return getattr(self, ABSOLUTE_URL_SLUG_FIELD) or self.id


    def __unicode__(self):
        return u"%s" %self.name

    class Meta:
        abstract = True

"""
from claires.catalog.models import *
print Group.objects.get(site_slug="___Sozdanie_pricheski92").breadcrumbs()
"""

class Group(Base):
    in_menu =  models.BooleanField(u"Показать в меню", default=True)
    parent = models.ForeignKey("self", null=True, blank=True, verbose_name="Родитель", related_name="child")
    seo = GenericRelation(Seo)


    class Meta:
        verbose_name = u"Раздел"
        verbose_name_plural = u"Разделы"

    def family(self):
        self.get_family()


    def is_root(self):
        return self.is_root_node()




    def siblings(self, include_self=False):
        return self.get_siblings(include_self).values(*QS_DEFAULT_VALUE_ARGS)

    def parents(self, ascending=False, include_self=False):
        return self.get_ancestors(ascending, include_self).filter(in_menu=True)

    def childs(self, include_self=False):
        return self.get_descendants(include_self).filter(in_menu=True)


    def get_parents(self, ascending=False, include_self=False):
        return self.get_ancestors(ascending, include_self)

    def get_childs(self, include_self=False):
        return self.get_descendants(include_self)


    def get_absolute_url(self):
        if self.siteslug:
            return u"/catalog/%s"%self.siteslug


    def breadcrumbs(self,include_self=False):
        res = []
        if include_self:
            res.append({
                "title": self.site_name,
                "url": self.get_absolute_url()
                }) 
        def bread(obj, res):
            if obj:
                res.append({
                    "title": obj.site_name,
                    "url": obj.get_absolute_url()
                    })
                if obj.parent:
                    bread(obj.parent, res)
            return res
        return bread(self.parent, res)


    

    def __unicode__(self):
        return u"%s" %self.name

    @property
    def get_seo(self):
        cur = self.seo.first()
        if cur:
            return cur
        else:
            parent = u"- {}".format(self.parent.site_name) if self.parent else u""
            title = u"Каталог {} - {}".format(parent, self.sitename.title())
            return {
                "title": title,
                "description":u"",
                "keywords":u""
            }





class Property(Base):
    value_type = models.PositiveIntegerField(u'Тип значений', default=1, choices=TYPE_IN)

    class Meta:
        verbose_name = u"Свойство товара"
        verbose_name_plural = u"Свойства товара"
    

class PropertyVariant(Base):
    prop = models.ForeignKey(Property, verbose_name=u"Свойства", null=True, blank=True, related_name="variants")
    variant_value = models.CharField(u"Текст", max_length=255, null=True, blank=True)

    class Meta:
        verbose_name = u"Вариант свойства"
        verbose_name_plural = u"Варианты свойства"
    

class ProductImages(models.Model):
    photo = models.ImageField("Картинка", null=True, blank=True, upload_to=settings.PRODUCTS_FOLDER)
    date_created = models.DateTimeField(u"Дата создания", default=datetime.datetime.now)

    class Meta:
        verbose_name = u"Фото изделий"
        verbose_name_plural = u"Фоты"

    def __unicode__(self):
        return u"%s" %self.photo




class Product(Base):
    barcode = models.CharField(u"Штрих код", max_length=255)
    art = models.CharField(u"Артикул", max_length=255)
    group = models.ForeignKey(Group, verbose_name=u"Раздел", null=True, blank=True,related_name="products")
    prop = models.ManyToManyField(PropertyVariant, verbose_name=u"Свойства", related_name="property")
    description = models.CharField(u"Описание", max_length=255, null=True, blank=True)
    images = GenericRelation(Image)
    seo = GenericRelation(Seo)
    quantity = models.PositiveIntegerField(u'Общее кол-во', default=0)
    stores = models.ManyToManyField("Stores",verbose_name=u"Магазины", through="ProductBalance", related_name="stores_list", limit_choices_to = {'is_show': True})
    price = models.ManyToManyField("Taxes", verbose_name=u"Цены", through="ProductTax", related_name="taxes")
    #categories = models.ManyToManyField("Category", verbose_name=u"Категории",null=True, blank=True, related_name="categ_products")

    class Meta:
        verbose_name = u"Товар"
        verbose_name_plural = u"Товары"


    def get_prop(self):
        return self.prop.filter(prop__is_show=True)

    def get_absolute_url(self):
        if self.group:
            return u"%s/%s"%(self.group.get_absolute_url(), self.siteslug)


    def get_price(self):
        if self.product_price.first():
            return self.product_price.first().price

    def get_old_price(self):
        price = self.product_price.filter(tax__default=False)
        if price:
            return price[0].price


    def get_stores(self):
        return self.product_stores.filter(store__is_show=True).order_by("store__city")

    @property
    def image(self):
        if self.images.first():
            return self.images.first()
        #return os.path.join(root("static"),'img','sample.jpg')

    @property
    def get_seo(self):
        cur = self.seo.first()
        if cur:
            return cur
        else: 
            return  {
                "title": u"{} - {} за {} руб.".format(self.group.get_seo["title"], self.sitename.title(), self.get_price()),
                "description": u"{}, {}".format(self.group, self.description),
                "text": u"{}".format(self.description),
                "keywords": u"{}".format(self.group)
            }
    @property
    def search_result(self):
        return {
            "name": self.site_name or self.name,
            "parent":self.group,
            "url": self.get_absolute_url(),
            "image":self.image,
        }


    def breadcrumbs(self):
        if self.group:
            return self.group.breadcrumbs(include_self=True)


    def __unicode__(self):
        return u"%s (%s)" %(self.art,self.sitename)


    def __str__(self):
        return u"%s" %self.art


class ProductBalance(Base):
    product = models.ForeignKey(Product, verbose_name=u"Товар", related_name="product_stores", limit_choices_to={'is_show': True})
    store = models.ForeignKey("Stores",verbose_name=u"Магазин", limit_choices_to={'is_show': True})
    quantity = models.PositiveIntegerField(u'Кол-во', default=0)

    class Meta:
        verbose_name = u"Наличие в магазинах"
        verbose_name_plural = u"Наличие в магазинах"

    def __unicode__(self):
        return u"%s:%s, %s шт." %(self.product.name, self.store.name, self.quantity)


    def quantity_list(self):
        return [None] * self.quantity

class ProductTax(Base):
    product = models.ForeignKey(Product, verbose_name=u"Товар", related_name="product_price", limit_choices_to={'is_show': True})
    tax = models.ForeignKey("Taxes",verbose_name=u"Цена", limit_choices_to={'is_show': True})
    price = models.PositiveIntegerField(u'Цена', default=0)

    class Meta:
        verbose_name = u"Цена"
        verbose_name_plural = u"Цены"

    def __unicode__(self):
        return u"%s:%s %s" %(self.product.name, self.price, self.tax.currency)

class Taxes(Base):
    currency = models.CharField(u"Валюта", max_length=255)
    default = models.BooleanField(u"По умолчанию", default=False)

    class Meta:
        verbose_name = u"Тип цены"
        verbose_name_plural = u"Типы цен"

    def __unicode__(self):
        return u"%s:%s" %(self.name, self.currency)

class Stores(Base):
    description = models.TextField(u"Описание",null=True, blank=True)
    address = models.CharField(u"Адрес", max_length=255, null=True, blank=True)
    geo = models.CharField(u"Гео", max_length=255, null=True, blank=True)
    seo = GenericRelation(Seo)
    city = models.ForeignKey(City,verbose_name=u"Город", null=True, blank=True)

    class Meta:
        verbose_name = u"Магазин"
        verbose_name_plural = u"Магазины"

    def __unicode__(self):
        return u"%s" %self.name

    def geo_array(self):
        try:
            geo = self.geo.split(",")
        except:
            return None
        if len(geo) == 2:
            return {
                "long": geo[1], "lat":geo[0]
        }

    @property
    def latitude(self):
        if self.geo_array():
            return self.geo_array()["lat"]

    @property
    def longitude(self):
        if self.geo_array():
            return self.geo_array()["long"]


    def googlemap_view(self):
        if self.geo_array():
            obj = {
                "lat": self.latitude,
                "lon": self.longitude,
                "title": u"%s, %s "%( self.city.name, self.site_name),
                "html": u"<h3>{0}</h3><p class='address'>{1}, {2}</p><p class='description'>{3}</p>".format(self.site_name, self.city.name, self.address, self.description),
                "icon": "/static/img/claires_.png",
                "animation": "google.maps.Animation.DROP",
                "zoom":15
            }
            return obj

    @property
    def search_result(self):
        return {
            "name": self.site_name or self.name,
            "parent":u"Магазины",
            "url": None,
            "image":none,
        }


class Exchange(models.Model):
    class Meta:
        verbose_name = 'Лог выгрузки'
        verbose_name_plural = 'Логи выгрузки'
    status_choices = {
        ('start', 'start'),
        ('progress', 'progress'),
        ('success', 'success'),
        ('error', 'error'),
    }

    exchange_type = models.CharField(max_length=50, choices=status_choices, default="start")
    timestamp = models.DateTimeField(auto_now_add=True)
    filename = models.CharField(max_length=200)
    comment = models.TextField(u"Comment", null=True, blank=True)


    def update_status(self, status):
        self.exchange_type = status

    @classmethod
    def log(cls, exchange_type, user, filename=u''):
        ex_log = Exchange(exchange_type=exchange_type, user=user, filename=filename)
        ex_log.save()




class Category(models.Model):
    site_name = models.CharField(u"Название", max_length=255, null=True, blank=True)
    site_slug = models.CharField(u"Ссылка (slug)", max_length=255, null=True, blank=True)
    in_top_menu = models.BooleanField(u"Показать в главном меню", default=True, help_text=u"Показать в главном верхнем меню сайта")
    in_menu = models.BooleanField(u"Показать в меню", default=True, help_text=u"Показать на странице категории в баннере(если есть)")
    parent = models.ForeignKey("self", null=True, blank=True, verbose_name="Родитель", related_name="child")
    seo = GenericRelation(Seo)
    banner = models.ImageField("Баннер категории", null=True, blank=True, upload_to="categories/banners")
    button_style = models.CharField(u"Стили кнопки",default="color:#000;background:#FFF;font-size:1em;", max_length=255, null=True, blank=True, help_text=u"color:#000;background:#FFF;font-size:1em;font-weight:normal;")
    products = models.ManyToManyField("Product",  null=True, blank=True, verbose_name=u"Товары", related_name="category_products", limit_choices_to = {'is_show': True})
    date_created = models.DateTimeField(u"Дата создания", default=datetime.datetime.now)

    class Meta:
        verbose_name = u"Категория"
        verbose_name_plural = u"Категории"

    def __unicode__(self):
        return u"%s" %self.site_name
        

    def family(self):
        self.get_family()


    def is_root(self):
        return self.is_root_node()

    def get_products(self):
        product_args = {"is_show": True}
        result = list()
        if self.childs():
            for child in self.childs():
                result.extend( map(lambda z: z, child.categ_products.filter(**product_args) ))
            return list(set(result))
        return self.categ_products.filter(**product_args)

    def siblings(self, include_self=False):
        return self.get_siblings(include_self).values("site_name","site_slug")

    def parents(self, ascending=False, include_self=False):
        return self.get_ancestors(ascending, include_self).filter(in_menu=True)

    def childs(self, include_self=False):
        return self.get_descendants(include_self).filter(in_menu=True)


    def get_parents(self, ascending=False, include_self=False):
        return self.get_ancestors(ascending, include_self)

    def get_childs(self, include_self=False):
        return self.get_descendants(include_self)

    def get_categories(self):
        if self.childs():
            return self.childs(include_self=False)
        return self.get_siblings(include_self=True).filter(in_menu=True)

    @property
    def siteslug(self):
        return getattr(self, ABSOLUTE_URL_SLUG_FIELD) or self.id

    def get_absolute_url(self):
        return u"/catalog/theme/%s"%self.siteslug


    def breadcrumbs(self,include_self=False):
        res = []
        if include_self:
            res.append({
                "title": self.site_name,
                "url": self.get_absolute_url()
                }) 
        def bread(obj, res):
            if obj:
                res.append({
                    "title": obj.site_name,
                    "url": obj.get_absolute_url()
                    })
                if obj.parent:
                    bread(obj.parent, res)
            return res
        return bread(self.parent, res)



    def get_seo(self):
        cur = self.seo.first()
        if cur:
            return cur

    def get_buttons(self):
        pass

"""

class CategoryProduct(models.Model):
    category = models.ForeignKey("Category", verbose_name=u"Категория", related_name="category") 
    product = models.ForeignKey("Product", verbose_name=u"Товар", related_name="cat_product")

    class Meta:
        verbose_name = u"Товар в категории"
        verbose_name_plural = u"Товары в категории"

    def __unicode__(self):
        return u"%s %s" %(self.product.art, self.product.site_name)
"""



mptt.register(Group,)
mptt.register(Category,)