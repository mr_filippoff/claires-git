# -*- coding: utf-8 -*-
from rest_framework import serializers
from rest_framework_cache.serializers import CachedSerializerMixin
from rest_framework_cache.registry import cache_registry
from .models import *
import json
from claires.images.models import Image
from claires.seo.models import Seo

class SeoItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = Seo
        fields = ("title","descriptiom","keywords", "text")


class ImageItemSerializer(CachedSerializerMixin):
    class Meta:
        model = Image
        fields = ("title","image","thumb", "normal")

class ObjectRelatedField(serializers.RelatedField):
    def to_representation(self, value):
        if value and isinstance(value, Image):
            serializer = ImageItemSerializer(value)
        elif value and isinstance(value, Seo):
            serializer = SeoItemSerializer(value)
        else:
            raise Exception('Unexpected type of tagged object')
        return serializer.data


class SubCategorySerializer(CachedSerializerMixin):
    class Meta:
        model = Group
        fields = ('id', 'cid', 'name', 'site_slug','site_name', 'date_created', 'parent','get_absolute_url')


class GroupSerializer(CachedSerializerMixin):
    parent = SubCategorySerializer(read_only=True)
    childs = SubCategorySerializer(many=True, read_only=True)

    class Meta:
        model = Group
        fields = ('id', 'cid', 'name','site_slug', 'site_name','parent', 'get_absolute_url', 'date_created', 'childs')



#serializers.ModelSerializer
class SubCategoryCatSerializer(CachedSerializerMixin):
    class Meta:
        model = Category
        fields = ('site_slug','site_name', 'parent','get_absolute_url')


class CategorySerializer(CachedSerializerMixin):
    parent = SubCategoryCatSerializer(read_only=True)
    childs = SubCategoryCatSerializer(many=True, read_only=True)

    class Meta:
        model = Category
        fields = ('id', 'site_slug', 'site_name','parent', 'get_absolute_url', 'childs')



class ProductTaxSerializer(CachedSerializerMixin):
    tax_cid = serializers.ReadOnlyField(source='tax.cid', read_only=True)
    tax_id = serializers.ReadOnlyField(source='tax.id', read_only=True)
    tax_site_name = serializers.ReadOnlyField(source='tax.site_name', read_only=True)
    tax_currency = serializers.ReadOnlyField(source='tax.currency', read_only=True)
    price = serializers.IntegerField()
    
    class Meta:
        model = ProductTax
        fields = ('tax_cid', 'tax_id', 'tax_site_name','tax_currency','price')


class ProductPropSerializer(CachedSerializerMixin):
    prop_id = serializers.ReadOnlyField(source='prop.id', read_only=True)
    prop_name = serializers.ReadOnlyField(source='prop.name', read_only=True)
    id =  serializers.ReadOnlyField()
    name =  serializers.ReadOnlyField()
    site_name =  serializers.ReadOnlyField()

    class Meta:
        model = PropertyVariant
        fields = ('prop_id', 'prop_name', 'id','name','site_name')


class ProductStoreSerializer(serializers.HyperlinkedModelSerializer):
    store_id = serializers.ReadOnlyField(source='store.id', read_only=True)
    store_cid = serializers.ReadOnlyField(source='store.cid', read_only=True)
    store_name =  serializers.ReadOnlyField(source='store.name')
    store_site_name = serializers.ReadOnlyField(source='store.site_name')
    store_city_name = serializers.ReadOnlyField(source='store.city.name')
    store_is_show = serializers.ReadOnlyField(source='store.is_show')
    quantity = serializers.IntegerField()

    class Meta:
        model = ProductBalance
        fields = ('store_name', 'store_cid', 'store_site_name','store_id','quantity','store_city_name','store_is_show')

class CitySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = City
        fields = ('name', 'slug', 'ordering','coord')


class StoresSerializer(CachedSerializerMixin):
    city = CitySerializer(read_only=True)
    googlemap_view = serializers. DictField()
    class Meta:
        model = Stores
        fields = ('id', 'cid', 'name', 'site_name', 'date_created', 'description', 'address', 'city', 'googlemap_view')


class ProductSerializer(CachedSerializerMixin):
    stores = serializers.SerializerMethodField(source="get_stores")
    price = ProductTaxSerializer(many=True, source='product_price')
    group = GroupSerializer(read_only=True)
    images = ObjectRelatedField(read_only=True, many=True)
    seo = ObjectRelatedField(read_only=True, many=True)
    prop = serializers.SerializerMethodField('get_props')
    url = serializers.URLField(source='get_absolute_url')

    class Meta:
        model = Product
        format = 'json'

    def get_props(self, container):
        props = container.prop.filter(is_show=True, prop__is_show=True)
        serializer = ProductPropSerializer(instance=props, many=True)
        return serializer.data

    def get_stores(self, container):
        aval_stores = container.product_stores.filter(store__is_show=True).order_by("store__city")
        serializer = ProductStoreSerializer(instance=aval_stores, many=True)
        return serializer.data


class PropertyVariantSerializer(CachedSerializerMixin):
    class Meta:
        model = PropertyVariant
        fields = ('id', 'cid', 'name', 'site_name', 'date_created',)

class PropSerializer(CachedSerializerMixin):
    variants = PropertyVariantSerializer(many=True)
    class Meta:
        model = Property
        fields = ('id', 'cid', 'name', 'site_name', 'date_created','variants')

cache_registry.register(PropSerializer)
cache_registry.register(PropertyVariantSerializer)
cache_registry.register(ProductSerializer)
cache_registry.register(StoresSerializer)
cache_registry.register(ProductPropSerializer)
cache_registry.register(GroupSerializer)



