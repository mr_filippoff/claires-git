# -*- coding: utf-8 -
from django.contrib import  admin
from django import forms
from django_mptt_admin.admin import DjangoMpttAdmin,MPTTModelAdmin
from django.contrib.contenttypes.admin import GenericTabularInline
from .models import *
from claires.images.models import Image
from claires.seo.models import Seo
from mptt.admin import DraggableMPTTAdmin

"""
class PageImage(GenericTabularInline):
    model = Image

class PageSeo(GenericTabularInline):
    model = Seo
"""


class DecadeBornListFilter(admin.SimpleListFilter):
    title = 'decade born'

    # Parameter for the filter that will be used in the URL query.
    parameter_name = 'decade'

    def lookups(self, request, model_admin):
        return (
            (0, 'Все'),
            (1, 'Главные разделы'),
        )

    def queryset(self, request, queryset):
        if self.value() == 1:
            return queryset.filter(parent=None).exclude("parent")




class BaseAdmin(admin.ModelAdmin):
    list_display = ("name",'site_name','site_slug', 'is_show')
    search_fields = ("name", "cid", "site_name","site_slug",)
    ordering= ('name','site_name','is_show',)
    readonly_fields = ('cid','name','date_created')
    list_editable = ('site_slug','site_name', 'is_show',)
    list_per_page = 500



class InlineImage(GenericTabularInline):
    model = Image
    readonly_fields = ('type_update',)


class InlineSeo(GenericTabularInline):
    model = Seo
    max_num = 1


class CatProductnline(admin.TabularInline):
    model = Product
    fields = ("art","site_name")
    """
    model = CategoryProduct
    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        kwargs["queryset"] = Product.objects.filter(is_show=True, art__isnull=False)
        return super(CatProductnline, self).formfield_for_foreignkey(db_field, request, **kwargs)
    """

class PropVariantsInline(admin.TabularInline):
    model = PropertyVariant


class StoresInline(admin.TabularInline):
    model = Stores


class ProductTaxInline(admin.TabularInline):
    model = ProductTax
    fields = ("tax",'price',)
    readonly_fields = fields
    max_num = 2
    verbose_name = u"Цены"
    can_delete=False
    classes = ('grp-collapse grp-open',)

class ProductBalanceInline(admin.TabularInline):
    model = ProductBalance
    fields = ("store",'quantity',)
    readonly_fields = ("store",'quantity',)
    verbose_name = u"Наличие в магазинах"
    can_delete=False
    classes = ('grp-collapse grp-open',)

class PropVariants(admin.TabularInline):
    model = PropertyVariant



class GroupFilter(admin.SimpleListFilter):
    parameter_name = 'level'
    title="Уровень вложенности"

    def lookups(self, request, model_admin):
        countries = [
            [0, "Главная категория"],
        ]
        return countries

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(level__lte=self.value())


class GroupAdmin(DraggableMPTTAdmin):
    inlines = [InlineSeo]
    list_display=(
        'indented_title',
        'site_slug',
        'site_name',
        'is_show',
        'in_menu'
    )
    list_filter = ["is_show","in_menu",GroupFilter]
    list_editable = list(BaseAdmin.list_editable)
    list_editable.extend(['in_menu'])

class PropertyAdmin(BaseAdmin):
    inlines =[PropVariantsInline]

class PropertyVariantAdmin(BaseAdmin):
    list_display = list(BaseAdmin.list_display)
    list_display.extend(['prop','variant_value'])

class ProductAdmin(BaseAdmin):
    readonly_fields = list(BaseAdmin.readonly_fields)
    exclude = ("prop",)
    list_display = list(BaseAdmin.list_display)
    list_display.extend(['barcode','art','quantity'])
    search_fields = list(BaseAdmin.search_fields)
    search_fields.extend(["art"])
    readonly_fields.extend(['quantity','art','barcode','group'])
    inlines = [InlineSeo, InlineImage, ProductTaxInline, ProductBalanceInline]
    list_filter = ["stores", "price", "group",]
    raw_id_fields = ('group','stores',)

    # define the related_lookup_fields
    related_lookup_fields = {
        'fk': ['group'],
        'm2m': ['stores'],
    }
    def formfield_for_dbfield(self, db_field, **kwargs):
        request = kwargs['request']
        field =  super(ProductAdmin, self).formfield_for_dbfield(db_field, **kwargs)
        if db_field.name == 'seo':
            field.title.initial = 'Title'
        if db_field.name == 'categories':
            choices = getattr(request, '_myfield_choices_cache', None)
            if choices is None:
                request._myfield_choices_cache = choices = list(field.choices)
            field.choices = choices


        return field


class StoresAdmin(BaseAdmin):
    list_display = list(BaseAdmin.list_display)
    list_display.extend(['address','city'])
    inlines = [InlineSeo]
    readonly_fields = ('cid','name',)


class TaxesAdmin(BaseAdmin):
    list_display = list(BaseAdmin.list_display)
    list_display.extend(['currency','default'])



class ProductsInLine(admin.TabularInline):
    model = Product





class CategForm(forms.ModelForm):
    csvfile = forms.FileField(
        label='Артикулы товаров',
        required=False,
        help_text = u"Текстовый файл (csv/txt). Артикулы расположите столбцом. При добавлении новых файлов, предыдущие не удаляются. Вы сможете их исключить из категории в форме выше - Товары"
    )
    class Meta:
        fields = "__all__"
        model = Category


class SiteCategoryAdmin(admin.ModelAdmin):
    inlines = [InlineSeo]
    filter_horizontal  = ('products',)
    list_filter = ["in_menu", "in_top_menu"]
    list_display = ["site_name",'site_slug','in_menu','in_top_menu','parent']
    list_editable = ('in_menu','in_top_menu','parent')
    prepopulated_fields = {"site_slug": ("site_name",)}
    form = CategForm

    class Media:
        css = {
             'all': ('/static/css/admin_fix.css',)
        }

    def save_related(self, request, form, formsets, change):
        super(SiteCategoryAdmin, self).save_related(request, form, formsets, change)
        obj  = form.instance
        data = form.cleaned_data['csvfile']
        if data:
            res = []
            for chunk in data.readlines():
                res.append(chunk.replace("\n","").replace("\r",""))
            products = Product.objects.filter(art__in=res, )
            if products.count():
                obj.products.add(*products)
                obj.save()


admin.site.register(Group,GroupAdmin)
admin.site.register(Property, PropertyAdmin)
admin.site.register(PropertyVariant, PropertyVariantAdmin)
admin.site.register(Product,ProductAdmin)
admin.site.register(Stores,StoresAdmin)
admin.site.register(Taxes, TaxesAdmin)
admin.site.register(Category, SiteCategoryAdmin)
