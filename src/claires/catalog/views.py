# -*- coding: utf-8 -*-
import json
import datetime
from django.shortcuts import get_object_or_404
from django.views.generic import View
from django.http import Http404, HttpResponse
from django.conf import settings
from claires.views import ClairesView
from claires.catalog.models import *
from claires.catalog.filter import *
from claires.catalog.serializers import *
from django.core import serializers



class ShopsView(ClairesView):
    template_name = "catalog/shops.html"

    def get_context_data(self, request, **kwargs):
        return {
           "API_KEY": settings.GOOGLE_API_KEY,
        }



class CatalogView(ClairesView):
    template_name = "catalog/category.html"

    def get_context_data(self, request, **kwargs):
        return {
            "groups": Group.objects.filter(is_show=True, parent=None)
        }


class CatalogCategoryView(CatalogView):

    def get_context_data(self, request, **kwargs):
        return {
            "group": get_object_or_404(Group, site_slug = kwargs.get("group_slug", None))    
        }



class CatalogCategoryPseudoView(CatalogView):
    def get_context_data(self, request, **kwargs):
        cat = get_object_or_404(Category, site_slug = kwargs.get("category_slug", None))    
        return {
            "category": cat,
            "group": cat,
        }


class CatalogProductView(CatalogView):
    template_name = "catalog/product.html"

    def get_context_data(self, request, **kwargs):
        item = get_object_or_404(Product, site_slug = kwargs.get("product_slug", None))
        return {
            "item": item,
            "siblings": Product.objects.filter(is_show=True, group = item.group,images__isnull=False).exclude(art=item.art).select_related()[:40],
            "groups": Group.objects.filter(is_show=True)
        }


