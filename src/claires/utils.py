# -*- coding: utf-8 -*-

import datetime
import re 
from ftplib import FTP
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.conf import settings
from django.conf import settings

def hash_str(string):
    a = hashlib.md5()
    a.update(str(string))
    return a.hexdigest()

def send_email_template(tpls={}, mail_dict={}, context={}):
    html_content = render_to_string(tpls["html"], context)
    text_content = render_to_string(tpls["text"], context)
    msg = EmailMultiAlternatives(mail_dict["subject"], text_content, mail_dict["sender"], mail_dict["to_mail"])
    msg.attach_alternative(html_content, "text/html")
    if msg.send(): return True   
    return False


def transliterate(string):
    capital_letters = {u'А': u'A',
                       u'Б': u'B',
                       u'В': u'V',
                       u'Г': u'G',
                       u'Д': u'D',
                       u'Е': u'E',
                       u'Ё': u'E',
                       u'Ж': u'Zh',
                       u'З': u'Z',
                       u'И': u'I',
                       u'Й': u'Y',
                       u'К': u'K',
                       u'Л': u'L',
                       u'М': u'M',
                       u'Н': u'N',
                       u'О': u'O',
                       u'П': u'P',
                       u'Р': u'R',
                       u'С': u'S',
                       u'Т': u'T',
                       u'У': u'U',
                       u'Ф': u'F',
                       u'Х': u'H',
                       u'Ц': u'Ts',
                       u'Ч': u'Ch',
                       u'Ш': u'Sh',
                       u'Щ': u'Sch',
                       u'Ъ': u'',
                       u'Ы': u'Y',
                       u'Ь': u'',
                       u'Э': u'E',
                       u'Ю': u'Yu',
                       u'Я': u'Ya',}
 
    lower_case_letters = {u'а': u'a',
                       u'б': u'b',
                       u'в': u'v',
                       u'г': u'g',
                       u'д': u'd',
                       u'е': u'e',
                       u'ё': u'e',
                       u'ж': u'zh',
                       u'з': u'z',
                       u'и': u'i',
                       u'й': u'y',
                       u'к': u'k',
                       u'л': u'l',
                       u'м': u'm',
                       u'н': u'n',
                       u'о': u'o',
                       u'п': u'p',
                       u'р': u'r',
                       u'с': u's',
                       u'т': u't',
                       u'у': u'u',
                       u'ф': u'f',
                       u'х': u'h',
                       u'ц': u'ts',
                       u'ч': u'ch',
                       u'ш': u'sh',
                       u'щ': u'sch',
                       u'ъ': u'',
                       u'ы': u'y',
                       u'ь': u'',
                       u'э': u'e',
                       u'ю': u'yu',
                       u'я': u'ya',
                       u' ': u'_',}
 
    translit_string = ""
 
    for index, char in enumerate(string):
        if char in lower_case_letters.keys():
            char = lower_case_letters[char]
        elif char in capital_letters.keys():
            char = capital_letters[char]
            if len(string) > index+1:
                if string[index+1] not in lower_case_letters.keys():
                    char = char.upper()
            else:
                char = char.upper()
        translit_string += char
 
    return translit_string



def set_cookie(response, key, value, days_expire = 7):
  if days_expire is None:
    max_age = 365 * 24 * 60 * 60  #one year
  else:
    max_age = days_expire * 24 * 60 * 60 
  expires = datetime.datetime.strftime(datetime.datetime.utcnow() + datetime.timedelta(seconds=max_age), "%a, %d-%b-%Y %H:%M:%S GMT")
  response.set_cookie(key, value, max_age=max_age, expires=expires, domain=settings.SESSION_COOKIE_DOMAIN, secure=settings.SESSION_COOKIE_SECURE or None)



#ftp_name - settings.FTP_EXCHANGE_SETTINGS's Name
#filename.xml
#data - {} for render in template
def xml_to_ftp(ftp_name, filename, data):
        ftp_settings = settings.FTP_EXCHANGE_SETTINGS
        FTP_NAME = ftp_settings.get(ftp_name, None)
        try:
            filepath = "{}/{}".format(FTP_NAME["LOCAL_FILEPATH"], filename)
            file_open = open(filepath, "w").write(
              render_to_string( FTP_NAME["TEMPLATE"], {"data": data} ).encode('utf-8')
            )
            ftp = FTP(ftp_settings["SERVER"]["HOST"], timeout=3)
            ftp.login(ftp_settings["SERVER"]["USER"], ftp_settings["SERVER"]["PASS"])
            map(lambda x:ftp.cwd(x), FTP_NAME["FTP_DIRS"])
            ftp.storlines("STOR {}".format(filename), open(filepath, "r"))
            ftp.close()
            return True
        except Exception, e:
            return False


def phone_clean(phone):
    digits = re.sub("[^0-9]", "", phone)
    if digits:
        return digits[1:] if digits.startswith( '7' ) else  digits[:10]
    return phone