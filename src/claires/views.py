# -*- coding: utf-8 -*-
import hashlib
import json
from random import  randrange
from django.core.cache import cache
from django.http import HttpResponse, HttpResponseRedirect
from django.template.context import RequestContext
from django.template.defaultfilters import slugify
from django.template.loader import render_to_string
from django.utils.cache import patch_vary_headers
from django.views.generic import TemplateView, View
from django.views.generic.base import TemplateResponseMixin
from django.utils.safestring import mark_safe
from django.shortcuts import render_to_response
from django.http import Http404, HttpResponse
from .banners.models import *


class ClairesView(TemplateView):
    template_name = "index.html"

    def get_context_data(self, request, **kwargs):
        return {
            #"topbanners": Parent.objects.filter(slider=True).order_by("order"),
            "banners": Parent.objects.filter(display=True).order_by("order")
        }

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(request, **kwargs)
        response = self.render_to_response(context)
        return response

    def post(self, request, *args, **kwargs):
        data = self.get_context_data(request, **kwargs)
        return HttpResponse(json.dumps(data),content_type="application/json")
