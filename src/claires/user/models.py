# -*- coding: utf-8 -*-
import datetime
import os
from django.db import  models
from django.conf import settings
from django.contrib.contenttypes.fields import GenericRelation
from tinymce.models import HTMLField
from filebrowser.fields import FileBrowseField

NOW = datetime.datetime.now()
TIMEDELTA = datetime.timedelta(days=90)

GENDER_IN = (
    ("f", u'Женский'),
    ("m", u'Мужской'),
)

class Anket(models.Model):    
    first_name = models.CharField(u"Имя", max_length=50)
    last_name = models.CharField(u"Фамилия", max_length=50)
    phone = models.CharField(u"Телефон",max_length=10)
    email = models.EmailField(u"E-mail", max_length=128)
    date_born = models.DateField(u'Дата рождения', auto_now_add=False, auto_now = False)
    city = models.CharField(u"Город", max_length=50)
    gender = models.CharField(u"Пол", max_length=1, choices=GENDER_IN, default=u"f")
    is_email_newsleter = models.BooleanField(u"Согласен получать Email рассылку", default=True)
    is_sms_newsleter = models.BooleanField(u"Согласен получать SMS рассылку", default=True)
    gift = models.ForeignKey("Gift", null=True, blank=True, related_name="anket")

    class Meta:
        verbose_name = u"Анкета"
        verbose_name_plural = u"Анкеты"

    def __unicode__(self):
        return u"%s %s"%(self.first_name, self.last_name)



class Gift(models.Model):
    name = models.CharField(u"Название формы",max_length=50)
    is_active = models.BooleanField(u"Регистрация активна", default=True)
    content = models.TextField(u"Текст (описание перед формой)", null=True, blank=True)
    post_content = models.TextField(u"Доп.текст (описание в конце формы)", null=True, blank=True)
    message_success = models.TextField(u"Сообщение об успешной регистрации", max_length=128, default=u"Отлично! Все получилось!")
    points =  models.PositiveIntegerField(u'Количество баллов', default=0)
    procent_points = models.PositiveIntegerField(u'Количество процентов(%)', default=0)
    expired_points_date = models.DateTimeField(u'Дата окончания действия баллов', auto_now_add=False, auto_now = False)
    valid_points_days =  models.PositiveIntegerField(u'Колличество дней действия баллов', default=31)
    image = FileBrowseField("Изображение на странице регистрации", max_length=200, directory="gift/image/", extensions=[".jpg",".png",".gif"], blank=True, null=True, help_text=u"Картинка под кнопкой 'Отправить'")
    popup = FileBrowseField("Изображение всплывающее(pop-up)", max_length=200, directory="gift/popup/", extensions=[".jpg",".png",".gif"], blank=True, null=True,help_text=u"При клике - переход на страницу регистрации")
    sticky = FileBrowseField("Узкий фиксированный баннер", max_length=200, directory="gift/sticky/", extensions=[".jpg",".png",".gif"], blank=True, null=True,help_text=u"Изображение под главным меню")

    def __unicode__(self):
        return u"%s" %self.name

    class Meta:
        verbose_name = u"Регистрация"
        verbose_name_plural = u"Регистрации"


    def get_absolute_url(self):
        return "/user/register/%s"%self.id