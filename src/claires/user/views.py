# -*- coding: utf-8 -*-
import json
import requests
import urllib2
import re 
import random 
import datetime 
import logging 
import ssl
from datetime import datetime as dt
from xml.etree.ElementTree import XML, Element
from django.shortcuts import get_object_or_404
from django.http import Http404, HttpResponse,HttpResponseRedirect,JsonResponse
from django.conf import settings
from claires.views import ClairesView
from claires.utils import xml_to_ftp, set_cookie, phone_clean
from .models import *
from .forms import *


REQUEST_URL_CARD = "https://partners001.valtera.ru/ClairesSiteDiscountCards/GetDiscountCardsByPhone.php?id={}"


def getErpXml_by_phone(phone):
    try:
        context = ssl._create_unverified_context()
        response = urllib2.urlopen(REQUEST_URL_CARD.format(phone), context=context, timeout=5)
        data = response.read()
        if data:
            doc = XML(data)
            return doc.getchildren()
    except Exception, e:
        return []



def phone_check(request):
    data = {
        "valid":False,
        "error":None,
        "phone":None,
    }
    if request.GET.get("tel", 0):
        data["phone"] = phone_clean(request.GET.get("tel"))
        is_anket = Anket.objects.filter(phone = data["phone"]).count()
        xml = getErpXml_by_phone(data["phone"])
        if is_anket:
            return JsonResponse(data)
        elif not len(xml):
            data["valid"] = True
        return JsonResponse(data)


class RegisterView(ClairesView):
    template_name = "user/register.html"

    def get_context_data(self, request, *args, **kwargs):
        form = AnketGiftForm()
        gift_id = kwargs.get("gift", None)
        if gift_id:
            gift = get_object_or_404(Gift, id = gift_id)
            form = AnketGiftForm(initial={"gift":gift.id})
            return {
                "gift":gift,
                "form":form
            }

    def get(self, request, *args, **kwargs):
        if not kwargs.get("gift"):
             return HttpResponseRedirect("/")
        return super(RegisterView, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {
                "done":  False,
                "cookie_key": "is_registered_user",
                "message":None
        }
        form = AnketGiftForm(request.POST)
        if form.is_valid():
            try:
                gift_object = Gift.objects.get(id  = request.POST.get("register_id",0))
            except Gift.DoesNotExist:
                data["message"] = u"Подарок не найден! Скорей всего акция завершена...:("
                return JsonResponse(data) 
            phone = form.cleaned_data['phone']
            is_anket = Anket.objects.filter(phone = phone).count()
            xml = getErpXml_by_phone(phone)
            if not is_anket and not len(xml):
                obj = form.save(commit=False)
                obj.gift = gift_object
                obj.save()
                if xml_to_ftp( "USER_ANKET", "{}.xml".format(obj.id), obj):
                #if True:
                    data["done"] = True
                    data["message"] = gift_object.message_success
                    _JsonResponse = JsonResponse(data) 
                    set_cookie(_JsonResponse, data["cookie_key"], True, 196)
                    return _JsonResponse
                else:
                    data["message"] = u"Не удалось соединиться с базой ERP для записи анкеты. Пожалуйста, попробуйте позже."
                    return JsonResponse(data) 
            else:
                data["message"] = u"Анкета с номером <b>%s<b/b> уже загружена на сайт. Пожалуйста, позвоните 8-800-707-58-50 для решения проблемы"%phone
                return JsonResponse(data)
        data["message"] = u"Форма имеет ошибки. Заполните корректно все поля,пожалуйста..."
        return JsonResponse(data)
        