# -*- coding: utf-8 -*-
from tinymce.widgets import TinyMCE
from django.contrib import admin
from django import forms
from .models import *


class GiftForm(forms.ModelForm):
    content = forms.CharField(label=u"Основной текст", widget=TinyMCE(attrs={'cols': 65, 'rows': 10}))
    post_content = forms.CharField(label=u"Дополнительный текст", widget=TinyMCE(attrs={'cols': 65, 'rows': 10}))
    message_success = forms.CharField(label=u"Текст об удачной регистрации", widget=TinyMCE(attrs={'cols': 35, 'rows':2}))
    class Meta:
        model = Gift
        fields="__all__"

class AnketAdmin(admin.ModelAdmin):
    list_display = ('first_name','last_name','phone','email', 'date_born','gender','is_email_newsleter','is_sms_newsleter','gift', )
    list_filter = ["gift","gender","is_email_newsleter", "is_sms_newsleter"]
    readonly_fields = ('first_name','last_name','phone','email', 'date_born','gender','is_email_newsleter','is_sms_newsleter','city', 'gift', )

class GiftAdmin(admin.ModelAdmin):
    list_display = ('name','points','procent_points','expired_points_date','valid_points_days','is_active' )
    form = GiftForm

admin.site.register(Anket,AnketAdmin)
admin.site.register(Gift,GiftAdmin)
