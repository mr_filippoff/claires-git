from django.conf.urls import patterns, url, include
from .views import *

urlpatterns = patterns('',
    url(r'register$', RegisterView.as_view(), {"gift":None} ),
    url(r'register/phone_check/$', phone_check),
    url(r'register/(?P<gift>[\d]+)', RegisterView.as_view()),
)

