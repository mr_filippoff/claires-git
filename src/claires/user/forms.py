# -*- coding: utf-8 -*-
from django import forms
from django.contrib.admin.widgets import AdminFileWidget
from django.forms.widgets import Textarea, TextInput, Select, HiddenInput, FileInput, ClearableFileInput,DateInput
from django.conf import settings
from claires.forms import HTML5RequiredMixin
from .models import *
from claires.utils import phone_clean

class AnketGiftForm(forms.ModelForm):
    date_born = forms.DateField(required=True, label=u"Дата рождения (ДД.ММ.ГГГГ)")
    agree = forms.BooleanField(label=u"С <a href=\"/pages/informaciya/programma-loyalnosti-claires-friends\" target=\"_blank\">правилами клуба</a> ознакомлен(а)", required = True, )
    phone = forms.CharField(label=u"Мобильный телефон",required = True)
    gender = forms.ChoiceField(label=u"Пол", choices=GENDER_IN, widget=forms.RadioSelect())
    #gift = forms.IntegerField(widget=forms.HiddenInput())
    
    class Meta:
        model = Anket
        fields = ("first_name", "last_name", "phone", "email", "date_born", "city", "gender", "is_email_newsleter", "is_sms_newsleter")
        exclude = ['gift']

    def clean_phone(self):
        return phone_clean( self.cleaned_data['phone'] )


class AnketRegisterForm(AnketGiftForm):
    def __init__(self, *args, **kwargs):
        super(AnketRegisterForm, self).__init__(*args, **kwargs)
        self.fields.pop("agree")
        