from django.conf import settings
from .catalog.models import *
from .pages.models import *
from .banners.models import *
from .user.models import *

def ctx_procc(request):
    gift = Gift.objects.filter(is_active=True)
    return {
        "top_menu": Group.objects.filter(is_show=True, in_menu=True, parent=None).order_by("tree_id"),
        "gift": gift[0] if gift.count() else [],
        "sections": PageSection.objects.filter(display=True).order_by("order"),
        "top_categories":Category.objects.filter(in_top_menu=True).order_by("date_created"),
        "global_banners":Banner.objects.filter(display_all_pages=True, display=True).order_by("order","created_date"),
        "settings":settings
    }