# -*- coding: utf-8 -*-
from django import forms
from django.contrib.admin.widgets import AdminFileWidget
from django.forms.widgets import Textarea, TextInput, Select, HiddenInput, FileInput, ClearableFileInput,DateInput
from django.conf import settings
from .models import *
from claires.forms import HTML5RequiredMixin

class AnkettForm(HTML5RequiredMixin):
    date_of_birth = forms.DateField(required=True, label="Дата рождения", widget = DateInput(format=('%d.%m.%Y'), attrs={'placeholder': u'дд.мм.гггг'}))
    class Meta:
        model = JobsApply
        fields="__all__"
        exclude = ["date_added","ip"]

