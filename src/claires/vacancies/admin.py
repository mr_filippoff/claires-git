# -*- coding: utf-8 -*-
from django.contrib import admin
from .models import JobsApply


class JobsApplyAdmin(admin.ModelAdmin):
    list_display = ('vacancy','FIO', 'date_of_birth',  'email', 'phone', 'date_added', 'cv')
    readonly_fields = ('vacancy', 'FIO', 'date_of_birth', 'address_of_registration',
                       'address_of_living', 'education', 'cash_register', 'marital_status',
                       'email', 'phone', 'computer_knowledge', 'experience','ip','date_added', )

admin.site.register(JobsApply, JobsApplyAdmin)