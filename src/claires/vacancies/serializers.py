# -*- coding: utf-8 -*-
from rest_framework import serializers
from rest_framework_cache.serializers import CachedSerializerMixin
from rest_framework_cache.registry import cache_registry
from .models import *
import json



class JobsApplySerializer(serializers.ModelSerializer):
    class Meta:
        model = JobsApply

cache_registry.register(JobsApplySerializer)



