# -*- coding: utf-8 -*-
import json
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.views.generic import TemplateView, View
from django.core.mail.message import EmailMessage

from claires.views import ClairesView
from claires.utils import send_email_template
from claires.pages.models import  PageSection, Page
from .forms import  AnkettForm
from .models import JobsApply


class VacanciesView(ClairesView):
    template_name = "vacancies/vacancies.html"


class AnkettFormView(ClairesView):
    template_name = "vacancies/ankett.html"

    def post(self, request, *args, **kwargs):
        form = AnkettForm(request.POST, request.FILES)
        context = {
            'form':form.as_ul()
        }
        if form.is_valid():
            obj = form.save()
            obj.save()

            email_dict = {
                "email_to": ["fgv@valtera.ru", "hr@valtera.ru"],
                "subject":u"Новая анкета www.claires.ru: %s"%obj.vacancy,
                "message": u"Здравствуй, дорогой сотрудник отдела кадров! <br />На сайте claires.ru человек заполнил анкету. Посмотреть ее можно по <a href='%s' target='_blank'>этой ссылке</a>"%obj.get_admin_full_url(),
                "email_from":u"noreply@valtera.ru",
            }
            email = EmailMessage(
                    email_dict["subject"], 
                    email_dict["message"], 
                    email_dict["email_from"], 
                    email_dict["email_to"]
                )
            email.content_subtype = "html"
            email.send()
            self.template_name = "vacancies/ankett_done.html"

        return self.response_class(request, self.get_template_name(), context)

    def get_context_data(self, request, **kwargs):
        form = AnkettForm(initial= {
            "vacancy": request.GET.get("vac",""),
            "cash_register":u"нет",
            "marital_status":3,
            "education":1,
            "computer_knowledge":4
        })
        return {
            'form':form.as_ul()
        }

    def get_template_name(self):
        return  self.template_name
