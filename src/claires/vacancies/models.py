# -*- coding: utf-8 -*-
import datetime
from django.db import models

JOBS_IN = (
    (1,u'Работа в магазинах'),
    (2,u'Работа в офисе'),
)


MARTIAL_IN = (
    (1,u'Не замужем/Не женат'),
    (2,u'Замужем/Женат'),
    (3,u'Разведен(а)'),
)

EDU_IN = (
    (1,u'Не имею'),
    (2,u'Среднее'),
    (3,u'Среднее специальное'),
    (4,u'Неоконченное высшее'),
    (4,u'Высшее'),
)

PC_EDU_IN = (
    (1,u'Профессионально (системный администратор, наладка оборудования и пр.)'),
    (2,u'Продвинутый пользователь (отличное знание пакета MS Office, 1C и т.д.)'),
    (3,u'Пользователь (базовые знания основных программ Windows и пакета MS Office)'),
    (4,u'Не работаю на ПК'),
)
"""
class Vacancies(models.Model):
    jobs_in = models.PositiveIntegerField(u'Работа в', default=1, choices=JOBS_IN)
    name = models.CharField(u'Краткое название вакансии', null=True, blank=True, max_length=60)
    short_description = models.CharField(u'Заголовок вакансии', null=True, blank=True, max_length=500)
    full_description = models.TextField(u'Полное описание вакансии', null=True, blank=True)
    resume = models.BooleanField(u'Послать резюме', default=False)
    ankett = models.BooleanField(u'Заполнить анкету', default=False)
    hide = models.BooleanField(u'В архиве', default=False)
    

    def __unicode__(self): 
        return self.full_name

    class Meta: 
        verbose_name,verbose_name_plural = u'Вакансия',u'Вакансии'

    def get_absolute_url(self):
        return "/vacancies/%s"%self.id

    def get_new_absolute_url(self):
        return "/vacancies/%s"%self.id
"""

class JobsApply(models.Model):
    vacancy = models.CharField(u'Вакансия', null=True, blank=True, max_length=200, help_text=u'Укажите вакансию')
    FIO = models.CharField(u'Фамилия Имя', null=True, blank=True, max_length=200)
    email = models.EmailField(u'E-mail', null=True, blank=True)
    phone = models.CharField(u'Мобильный телефон', null=True, blank=True, max_length=21)
    date_of_birth = models.DateField(u"Дата рождения")
    address_of_registration = models.CharField(u'Адрес постоянной регистрации', null=True, blank=True, max_length=300)
    address_of_living = models.CharField(u'Адрес проживания', null=True, blank=True, max_length=300)
    cash_register = models.CharField(u'Опыт работы с кассовым аппаратом (каким)', null=True, blank=True, max_length=300)
    marital_status = models.PositiveIntegerField(u'Семейное положение', default=1, choices=MARTIAL_IN)
    computer_knowledge = models.PositiveIntegerField(u'Степень владения ПК', default=1, choices=PC_EDU_IN)
    education = models.PositiveIntegerField(u'Образование', default=2, choices=EDU_IN)
    experience = models.TextField(u'Опыт работы', null=True, blank=True)
    cv = models.FileField(u'Резюме', upload_to='jobs/', null=True, blank=True, help_text=u"Если у вас есть готовое резюме, загрузите его здесь")
    date_added = models.DateTimeField(u'Дата создания', default=datetime.datetime.now)
    ip = models.GenericIPAddressField(protocol="IPv4", default="127.0.0.1")


    def __unicode__(self): 
        return u"%s - %s"%(self.FIO, self.vacancy)

    def get_admin_full_url(self):
        return u"http://www.claires.ru/admin/vacancies/jobsapply/%s/change/"%self.id

    class Meta: verbose_name,verbose_name_plural = u'Анкета',u'Анкеты'


