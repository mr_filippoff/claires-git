# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from .views import *

urlpatterns = patterns('',
    url(r'form$', AnkettFormView.as_view(), name="ankett"),
    url(r'$', VacanciesView.as_view(), name="vacancies"),
)