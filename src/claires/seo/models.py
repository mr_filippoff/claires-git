#-*- coding: utf-8 -*-
from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey
import os


class Seo(models.Model):
    title = models.CharField(u"Заголовок", max_length=250)
    descriptiom = models.CharField(u"Описание", max_length=250,null=True, blank=True)
    keywords = models.CharField(u"Ключевые слова", max_length=250, null=True, blank=True)
    text = models.TextField(u"Содержание", null=True, blank=True)
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')

    class Meta:
        verbose_name = u"SEO"
        verbose_name_plural = u"SEO описание"

    def __unicode__(self):
        return u"%s" %self.title