# -*- coding: utf-8 -
from django.contrib import  admin
from .models import *

class ImageAdmin(admin.ModelAdmin):
    list_display = ("title",'display','order','content_type')
    search_fields = ("title","content_type")
    ordering= ('title','order','display',)
    list_filter = ["content_type", "display"]
    list_per_page = 500

admin.site.register(Image, ImageAdmin)