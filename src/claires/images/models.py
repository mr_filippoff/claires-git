#-*- coding: utf-8 -*-
from django.db import models
from claires.utils import transliterate
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey
import os
from django.conf import settings
from django.db.models.signals import pre_delete
from django.dispatch.dispatcher import receiver
from sorl.thumbnail import get_thumbnail

TYPE_IN = (
    ("a", u'Автообновление'),
    ("m", u'Обычное'),
)


def generate_filename(instance, filename):
    f, ext = os.path.splitext(filename)
    return '%s/%d/%s%s' % (instance.content_type.app_label, instance.object_id, transliterate(instance.title), ext)   

class Image(models.Model):
    title = models.CharField(u"Имя картинки", max_length=250)
    image = models.ImageField(upload_to=generate_filename, null=True, blank=True)
    display = models.BooleanField(u"Виден?", default=True)
    order = models.IntegerField(u"Порядок", null=True, blank=True, default=1)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')
    type_update = models.CharField(u"Тип", max_length=1, choices = TYPE_IN, default="m")

    class Meta:
        verbose_name = u"Изображение"
        verbose_name_plural = u"Изображения"


    def thumb(self):
        if self.image:
            return self.image_resize()

    @property
    def normal(self):
        if self.image:
            return self.image_resize(size="500x500")

    def image_resize(self, size="274x274"):
        return os.path.join(settings.MEDIA_URL, get_thumbnail(self.image, size, crop='center', quality=100).name)

    def __unicode__(self):
        return u"%s" %self.image.url
    
    def get_absolute_url(self):
        return u"%s" %self.image.url

    def delete(self, *args, **kwargs):
        storage, path = self.image.storage, self.image.path
        super(Image, self).delete(*args, **kwargs)
        storage.delete(path)