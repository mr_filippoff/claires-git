# -*- coding: utf-8 -*-
from django.conf.urls.static import static
from filebrowser.sites import site
try:
    from django.conf.urls import patterns, url, include
except ImportError:
    from django.conf.urls.defaults import *
from django.conf import  settings
from django.contrib import admin
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse
from django.views.generic import RedirectView, TemplateView,ListView
from django.http import Http404, HttpResponse
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.contrib.staticfiles import views
from .views import *

urlpatterns = patterns('',
  url(r'^(?P<tpl>[\w\d\-\.]+)', PartialGroupView.as_view(template_name='partials/partial-template1.html'), name='partial_template1'),
)

