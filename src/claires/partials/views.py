# -*- coding: utf-8 -*-
from django.shortcuts import get_object_or_404
from django.views.generic import View
from django.http import Http404, HttpResponse
from django.conf import settings
from django.views.generic import TemplateView, View

class PartialGroupView(TemplateView):
    def get_context_data(self, **kwargs):
        if kwargs.get("tpl", None):
            self.template_name = "partials/%s"%kwargs.get("tpl")
        context = super(PartialGroupView, self).get_context_data(**kwargs)
        return context