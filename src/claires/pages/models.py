#-*- coding: utf-8 -*-
from django.db import  models
from claires.images.models import Image
from claires.seo.models import Seo
from django.contrib.contenttypes.fields import GenericRelation
from tinymce.models import HTMLField

class PageBase(models.Model):
    name = models.CharField(u"Название",max_length=50)
    slug = models.CharField(u"Имя ссылки", max_length=50)
    order = models.IntegerField(u"Порядок", null=True, blank=True, default=1)
    content = models.TextField(u"Содержание", null=True, blank=True)
    display = models.BooleanField(u"Показывать на сайте", default=True)
    redirect_to = models.CharField(u"Редирект", null=True, blank=True,max_length=255)

    class Meta:
        abstract = True



class PageSection(PageBase):
    images = GenericRelation(Image)
    seo = GenericRelation(Seo)
    class Meta:
        verbose_name = u"Раздел"
        verbose_name_plural = u"Разделы"
    
    def __unicode__(self):
        return u"%s" %self.name

    def pages(self):
        return self._pages.filter(display=True).order_by("order")

    def get_absolute_url(self):
        return "/pages/%s/"%self.slug



class Page(PageBase):
    section = models.ForeignKey(PageSection, verbose_name=u"Раздел страницы", related_name="_pages")
    annotate = models.CharField(u"Название", max_length=240,null=True, blank=True)
    created_date =  models.DateTimeField(u'Дата создания', auto_now_add=True, auto_now = False)
    pub_date =  models.DateTimeField(u'Дата публикации', auto_now_add=True, auto_now = False)
    images = GenericRelation(Image)
    seo = GenericRelation(Seo)

    @staticmethod
    def autocomplete_search_fields():
        return ("id__iexact", "name__icontains", "slug__icontains","content__icontains")

    class Meta:
        verbose_name = u"Страница"
        verbose_name_plural = u"Страницы"

    def __unicode__(self):
        return u"%s" %self.name

    @property
    def get_absolute_url(self):
        if self.redirect_to:
            return "%s"%self.redirect_to
        return "%s%s"%(self.section.get_absolute_url(), self.slug)


    def image(self):
        if self.images.first():
            return self.images.first().name

    def related_label(self):
        return u"%s (%s)" % (self.name, self.id)

    @property
    def search_result(self):
        return {
            "name": self.name,
            "parent":self.section,
            "url": self.get_absolute_url(),
            "image":self.image,
        }
