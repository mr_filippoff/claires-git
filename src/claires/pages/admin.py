from django.contrib import  admin
from django import forms
from claires.images.models import Image
from claires.seo.models import Seo
from .models import *
from django.contrib.contenttypes.admin import GenericTabularInline
from tinymce.widgets import TinyMCE
from django.conf import settings


class PageForm(forms.ModelForm):
    content = forms.CharField(widget=TinyMCE(attrs={'cols': 80, 'rows': 20}))

    class Meta:
        model = Page
        fields="__all__"



class PageSectionForm(forms.ModelForm):
    content = forms.CharField(widget=TinyMCE(attrs={'cols': 80, 'rows': 20}))

    class Meta:
        model = PageSection
        fields="__all__"


class PageImage(GenericTabularInline):
    model = Image

class PageSeo(GenericTabularInline):
    model = Seo

class PageSectionAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('name',)}
    list_display = ('name','slug','display',)
    form = PageSectionForm

class PageAdmin(admin.ModelAdmin):
    list_display = ("name",'section','display','order')
    list_editable = ["section", "display", "order"]
    prepopulated_fields = {'slug': ('name',)}
    search_fields = ("name",)
    ordering= ('section','order',)
    inlines = [PageImage,PageSeo,]
    form = PageForm

admin.site.register(Page,PageAdmin)
admin.site.register(PageSection,PageSectionAdmin)


