# -*- coding: utf-8 -*-
import json
from django.shortcuts import get_object_or_404
from django.views.generic import View
from django.http import Http404, HttpResponse
from django.conf import settings
from claires.views import ClairesView
from .models import  PageSection, Page
from djpjax import PJAXResponseMixin


class PageView(ClairesView):

    template_name = "pages/page.html"
    def get_context_data(self, request, **kwargs):
        context = {}
        section = get_object_or_404(PageSection, slug = kwargs.get("parent",None))
        context["page"] = section
        context["in_menu"] = section.pages
        if kwargs.get("page_slug",None):
            context["page"] = get_object_or_404(Page, section = section, display=True, slug = kwargs.get("page_slug",None))
            context["in_menu"] = context["page"].section.pages
        return context
