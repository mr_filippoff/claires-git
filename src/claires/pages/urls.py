from django.conf.urls import patterns, url, include
from .views import PageView

urlpatterns = patterns('',
    url(r'(?P<parent>[\w\d\-]+)/(?P<page_slug>[\w\d\-]+)', PageView.as_view()),
    url(r'(?P<parent>[\w\d\-]+)/$', PageView.as_view()),
)

