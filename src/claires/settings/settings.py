# -*- coding: utf-8 -*-

import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

here = lambda *x: os.path.join(os.path.abspath(os.path.dirname(__file__)), *x)

REPOSITORY_ROOT = here('..', '..', '..')

root = lambda *x: os.path.join(os.path.abspath(REPOSITORY_ROOT), *x)


# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '$@$#t-no0gh24rta4nfd37%n8(nsl9#*x#8kdnko)_'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ["*"]

SITE_ID = 1

STATICFILES_DIRS = ('static', )


# Database
# https://docs.djangoproject.com/en/1.9/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME':  'claires.sqlite3',
    }
}

CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://127.0.0.1:6379/1",
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
            "COMPRESSOR": "django_redis.compressors.zlib.ZlibCompressor",
            "IGNORE_EXCEPTIONS": True,
        },
        "KEY_PREFIX": "claires",
        "TIMEOUT ": 60*60
    },
    "rest": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://127.0.0.1:6379/2",
        "TIMEOUT ": 60*60*20,
        "KEY_PREFIX": "claires-rest"
    },
    "thumbs": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://127.0.0.1:6379/3",
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
        },
        "KEY_PREFIX": "claires-thumbs",
        "TIMEOUT ": 60*60*48
    }
}

INSTALLED_APPS = [
    'grappelli.dashboard', 
    'grappelli',
    'filebrowser',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sites',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'claires.search',
    'claires.pages',
    'claires.banners',
    'claires.catalog',
    'claires.images',
    'claires.seo',
    'claires.locations',
    'claires.vacancies',
    'claires.import_manager',
    'claires.user',
    'rest_framework',
    'rest_framework_extensions',
    'tinymce',
    'sorl.thumbnail',
    'bootstrap3',
    'mptt',
    'django_mptt_admin',
    'compressor',
    'djng',
]

MIDDLEWARE_CLASSES = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'claires.settings.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.static',
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'claires.context_proccesors.ctx_procc'
            ],
        },
    },
]

WSGI_APPLICATION = 'wsgi.application'



# Password validation
# https://docs.djangoproject.com/en/1.9/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/1.9/topics/i18n/

LANGUAGE_CODE = 'ru-RU'

TIME_ZONE = 'Europe/Moscow'

USE_I18N = True

USE_L10N = True

USE_TZ = True

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'compressor.finders.CompressorFinder',
)


LOGGING = {
    'version': 1,
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s | %(pathname)s:%(lineno)d (in %(funcName)s) | %(message)s '
        },
        'simple': {
            'format': '%(levelname)s %(funcName)s %(message)s'
        },
        },


    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'logfile': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': root('claires.log'),
            'maxBytes': 1024 ** 2,
            'backupCount': 5,
            'formatter': 'verbose',
        },
        'db_log': {
            'filename': root('db_claires.log'),
            'class': 'logging.handlers.RotatingFileHandler',
            'maxBytes': 1024 ** 2*10,
        }
    },
    'loggers': {
        'django.db.backends': {
            'handlers': ['db_log'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
        'import_manager': {
            'handlers': ['logfile'],
            'level': 'INFO'
        },
        'views': {
            'handlers': ['logfile'],
            'level': 'INFO'
        },

    },
}
FILE_UPLOAD_MAX_MEMORY_SIZE = 100000 * 1024 * 1024

STATIC_ROOT = root('collect_static')

MEDIA_ROOT = root('media')

STATIC_URL = "/collect_static/"


MEDIA_URL = "/media/"




EMAIL_BACKEND = 'claires.email.EmailBackend'
EMAIL_HOST_USER = 'noreply@valtera.ru'
EMAIL_HOST_PASSWORD = 'eddb0815dda'
EMAIL_HOST = 'lnd4.casket.ru'
EMAIL_USE_TLS = True
EMAIL_PORT = 25


#GRAPPELLI CONF
GRAPPELLI_ADMIN_TITLE = "CLAIRES ADMIN"
GRAPPELLI_INDEX_DASHBOARD = 'dashboard.CustomIndexDashboard'

#TYNY
STATIC_JS_DIR = os.path.join(STATIC_URL, "js")
TINYMCE_JS_ROOT = os.path.join(STATIC_JS_DIR, "_tiny_mce")
TINYMCE_JS_URL = os.path.join(TINYMCE_JS_ROOT, "tiny_mce.js")
TINYMCE_DEFAULT_CONFIG = {
    "language": "ru",
    "mode" : "textareas",
    "height":400,
    "relative_urls": "false",
    "paste_remove_spans": "true",
    "removeformat_selector" : "b,strong,em,i,span,ins",
    "file_browser_callback": "djangoFileBrowser",
    "theme_advanced_toolbar_location" : "top",
    "plugins" : "paste,table,save,advhr,advimage,advlink,iespell,insertdatetime,preview,searchreplace,print,contextmenu,fullscreen",
    "content_css": '/collect_static/css/tiny.content.css'
}
#TINYMCE_COMPRESSOR = True

#CATALOG
PRODUCT_DIR =  os.path.join(MEDIA_ROOT,  "products")
PRODUCT_UPLOAD_DIR =  os.path.join(MEDIA_ROOT,  "catalog_uploads")
PRODUCTS_FOLDER = "products"
PRODUCTS_FOLDER_ROOT = os.path.join(MEDIA_ROOT, PRODUCTS_FOLDER)


GOOGLE_API_KEY="AIzaSyDghSBpz0XJIzD2GdQjHEf449vTMZCa5hU"

REST_FRAMEWORK = {
    'PAGE_SIZE': 10,
    'DEFAULT_FILTER_BACKENDS': ('rest_framework.filters.DjangoFilterBackend',),
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.LimitOffsetPagination',
    #'DEFAULT_RENDERER_CLASSES': ('rest_framework.renderers.JSONRenderer',),
}

#django-compressor settings
COMPRESS_ENABLED = True
COMPRESS_REBUILD_TIMEOUT = 60*5
if DEBUG:
    COMPRESS_DEBUG_TOGGLE = 'clean'
    COMPRESS_REBUILD_TIMEOUT = 60
COMPRESS_OFFLINE =True
COMPRESS_CSS_FILTERS = ['compressor.filters.css_default.CssAbsoluteFilter',  'compressor.filters.cssmin.CSSMinFilter']

REST_FRAMEWORK_CACHE = {
    'DEFAULT_CACHE_BACKEND': 'rest',
    'DEFAULT_CACHE_TIMEOUT': 86400
}


VACANCIES_UPLOAD_FILE_TYPES = ['pdf', 'vnd.oasis.opendocument.text','vnd.ms-excel','msword','application']
VACANCIES_UPLOAD_FILE_MAX_SIZE = 5242880

"""
create database claires CHARACTER SET utf8 COLLATE utf8_general_ci;
CREATE USER 'claires'@'localhost' IDENTIFIED BY 'Heisenberg';
GRANT ALL PRIVILEGES ON * . * TO 'claires'@'localhost';

"""

VALTERA_FTP_HOST = 'ftp.casket.ru'
VALTERA_FTP_USER = 'ftp_1c'
VALTERA_FTP_PASS = 'QwesdF132'

FTP_EXCHANGE_SETTINGS = {
    "SERVER":{
        "HOST":'ftp.casket.ru',
        "PASS":'QwesdF132',
        "USER":'ftp_1c'
    },
    "USER_ANKET":{
        "TEMPLATE": "user/user_anket_tpl.xml",
        "LOCAL_FILEPATH":root("data/ankets/"),
        "FTP_DIRS":["ankets","claires_test"],
    }
}