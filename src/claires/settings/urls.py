# -*- coding: utf-8 -*-
from django.conf.urls.static import static
from filebrowser.sites import site
try:
    from django.conf.urls import patterns, url, include
except ImportError:
    from django.conf.urls.defaults import *
from django.conf import  settings
from django.contrib import admin
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse
from django.views.generic import RedirectView, TemplateView,ListView
from django.http import Http404, HttpResponse
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.contrib.staticfiles import views
from claires.views import ClairesView



admin.autodiscover()

urlpatterns = patterns('',
    url(r'^grappelli/', include('grappelli.urls')),
    url(r'^admin/filebrowser/', include(site.urls)),
    url(r'^admin/tinymce/', include('tinymce.urls')),
    url(r'^tinymce/', include('tinymce.urls')),
    url(r'^admin/',  include(admin.site.urls)),
    url(r'^export/',  include('claires.import_manager.urls')),
    url(r'^catalog/', include('claires.catalog.urls')),
    url(r'^job/', include('claires.vacancies.urls')),
    url(r'^user/', include('claires.user.urls')),
    url(r'^shops/', include('claires.catalog.shop_urls')),
    url(r'^pages/', include('claires.pages.urls')),
    url(r'^search/', include('claires.search.urls')),
    url(r'^api/',  include('claires.api.urls')),
    url(r'^tpl/',  include('claires.partials.urls')),
)


if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)


urlpatterns += patterns('', url(r'$', ClairesView.as_view()) )

urlpatterns += staticfiles_urlpatterns()
"""
urlpatterns = patterns('',
    url(r'^admin/',  include(admin.site.urls)),

    )
"""