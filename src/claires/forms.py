# -*- coding: utf-8 -*-
from django import forms
from django.contrib.admin.widgets import AdminFileWidget
from django.forms.widgets import Textarea, TextInput, Select, HiddenInput, FileInput, ClearableFileInput,DateInput


class HTML5RequiredMixin(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(HTML5RequiredMixin, self).__init__(*args, **kwargs)
        for field in self.fields:
            if type(self.fields[field].widget) not in (AdminFileWidget, HiddenInput, FileInput, ClearableFileInput):
                self.fields[field].widget.attrs['required'] = 'required'
                if self.fields[field].label:
                    self.fields[field].label += ' *'
