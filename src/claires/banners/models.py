#-*- coding: utf-8 -*-
from django.db import  models
from claires.images.models import Image
from claires.seo.models import Seo
from django.contrib.contenttypes.fields import GenericRelation
from tinymce.models import HTMLField
from filebrowser.fields import FileBrowseField


SIZES = (
    (12, "100%"),
    (11, "91%"),
    (10, "83%"),
    (9, "75%"),
    (8, "66%"),
    (7, "58%"),
    (6, "50%"),
    (5, "42%"),
    (4, "33%"),
    (3, "25%"),
    (2, "16%"),
    (1, "9%"),
)

class Banner(models.Model):
    name = models.CharField(u"Название",max_length=50)
    image = FileBrowseField("Изображение", max_length=200, directory="banners/", extensions=[".jpg",".png",".gif"], blank=True, null=True)
    url = models.CharField(u"Ссылка", max_length=255,blank=True, null=True)
    size =  models.PositiveIntegerField(u'Размер блока на странице', default=12, choices=SIZES)
    created_date = models.DateTimeField(u'Дата создания', auto_now_add=True, auto_now = False)
    display = models.BooleanField(u"Включен", default=True, help_text=u"Баннер активен и выводится.")
    display_all_pages = models.BooleanField(u"Включен на всех страницах", default=False,  help_text=u"Баннер выводится в самом верху, под главным меню. Даже на главной странице!")
    order = models.IntegerField(u"Порядок", null=True, blank=True, default=1)
    content = models.TextField(u"Содержание баннера (HTML)", null=True, blank=True)
    utm = models.BooleanField(u"Авто-Google Analytics метка",  default=True, help_text=u"Ссылка для баннера будет с utm меткой. Выключите, если указываете в ссылке свою utm метку")
    parent = models.ForeignKey("Parent", null=True, blank=True, related_name="banners")


    class Meta:
        verbose_name = u"Баннер"
        verbose_name_plural = u"Баннеры"

    def __unicode__(self):
        return u"%s" %self.name


class Parent(models.Model):
    name = models.CharField(u"Название",max_length=50)
    slug = models.CharField(u"Slug", max_length=255)
    order = models.IntegerField(u"Порядок", null=True, blank=True, default=0)
    auto_size = models.BooleanField(u"Авто-ширина", default=True, help_text=u"Автоматически изменится размер баннеров на основе размера блока. Это значит, что не важно какого размера у вас баннеры, они \"сожмутся\" в этой группы баннеров. Отключите это, если у вас точные размеры баннера")
    display = models.BooleanField(u"Включен", default=True, help_text=u"Блок с баннерами активен")
    slider = models.BooleanField(u"Слайдер", default=True, help_text=u"Баннеры-сладеры: если баннеров в группе > 1, то по бокам появляются стрелки для листания.Ширина и высота подгоняются под ширину сайта (т.е. эквивалентно включенной опции \"Авто-ширина\")")

    def __unicode__(self):
        return u"%s" %self.name

    class Meta:
        verbose_name = u"Группа баннеров"
        verbose_name_plural = u"Группы баннеров"

    def get_banners(self):
        return self.banners.filter(display=True).order_by("order","created_date")
