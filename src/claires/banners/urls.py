from django.conf.urls import patterns, url, include
from claires.views import ClairesView
from .views import PageView

urlpatterns = patterns('',
    url(r'(?P<section>[\w\d\-]+)/(?P<page_id>\d+)$', PageView.as_view()),
    url(r'(?P<section>[\w\d\-]+)/', PageView.as_view()),
    url(r'$', ClairesView.as_view()),
)

