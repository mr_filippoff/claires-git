# -*- coding: utf-8 -*-
import json
from django.shortcuts import get_object_or_404
from django.views.generic import View
from django.http import Http404, HttpResponse
from django.conf import settings
from claires.views import ClairesView
from .models import  PageSection, Page



class PageView(ClairesView):
    template_name = "pages/page.html"

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(request, **kwargs)
        response = self.render_to_response(context)
        return response

    def get_context_data(request, *args, **kwargs):
        section  = get_object_or_404(PageSection, slug = kwargs['section'])
        return {
            'page': get_object_or_404(Page, id = kwargs['page_id'], section = section )
        }