# -*- coding: utf-8 -
from django.contrib import  admin
from .models import *
from tinymce.widgets import TinyMCE
from django.conf import settings
from filebrowser.settings import ADMIN_THUMBNAIL


class BannersInline(admin.TabularInline):
    model = Banner
    fields = ("name",'url','order','parent','size', 'image','display_all_pages','display')
    verbose_name = u"Баннеры"
    classes = ('grp-collapse grp-closed',)
    ordering = ('parent', )
    extra = 1


class BannerAdmin(admin.ModelAdmin):
    list_display = ("name",'image_thumbnail', 'url','order','parent',"size","display_all_pages","display")
    list_filter = ["parent","display"]
    list_editable = ["parent","size", "display", "display_all_pages", "order", "url"]


    def image_thumbnail(self, obj):
        if obj.image and obj.image.filetype == "Image":
            return '<img src="%s" />' % obj.image.version_generate(ADMIN_THUMBNAIL).url
        else:
            return ""
    image_thumbnail.allow_tags = True
    image_thumbnail.short_description = "Превью"



class ParentBannerAdmin(admin.ModelAdmin):
    list_display = ("name","display","order","slug","image_thumbnail","slider")
    list_filter = ["slider","display"]
    list_editable = ["order","display"]
    inlines=[BannersInline]
    prepopulated_fields = {"slug": ("name",)}
    ordering = ('order', )

    def image_thumbnail(self, obj):
        if obj.get_banners():
            thumbs = []
            for banner in obj.get_banners():
                if banner.image.filetype == "Image":
                    thumbs.append(
                         '<img src="%s" />' % banner.image.version_generate(ADMIN_THUMBNAIL).url
                    )
            return u"&nbsp;".join(thumbs)
        else:
            return ""
    image_thumbnail.allow_tags = True
    image_thumbnail.short_description = "Баннеры"


admin.site.register(Banner,BannerAdmin)
admin.site.register(Parent,ParentBannerAdmin)