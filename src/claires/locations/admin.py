from django.contrib import admin
from .models import City

class CityAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("name",)}
    list_display = ('id','name','slug','ordering', )
    list_editable = ["ordering","slug"]

admin.site.register(City,CityAdmin)
