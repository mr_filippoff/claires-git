# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext_lazy as _


class City(models.Model):
    name = models.CharField(_("City"), max_length=50 )
    slug = models.SlugField(unique=True)
    ordering = models.IntegerField(_("Ordering"), default=9999)
    coord = models.CharField(_("Coordinates"), max_length=50, null=True, blank=True)

    class Meta:
        verbose_name = _("City")
        verbose_name_plural = _("Cities")


    def natural_key(self):
        return (self.slug, self.name)

    def __unicode__(self):
        return u"%s" %self.name
