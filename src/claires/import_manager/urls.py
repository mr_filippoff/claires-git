try:
    from django.conf.urls import patterns, url, include
except ImportError:
    from django.conf.urls.defaults import *
from . import views

urlpatterns = [
    url(r'^exchange$', views.front_view, name='front_view'),
]
