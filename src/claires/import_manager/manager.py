# -*- coding: utf-8 -*-
from xml.dom.minidom import parse
import xml.dom.minidom
import os 
import re
import zipfile
import io
import logging
import importlib
from io import BytesIO
from decimal import Decimal
from datetime import datetime
from django.conf import settings
from claires.catalog.models import *
try:
    from PIL import Image
    from PIL import ImageFile
    from PIL import ImageFilter
    from PIL import ImageEnhance
except ImportError:
    raise ImportError('Unable to import the Python Imaging Library. Please confirm it`s installed and available on your current Python path.')

from django.core.files import File
from claires.images.models import Image
from django.contrib.contenttypes.models import ContentType
from sorl.thumbnail import get_thumbnail


logger = logging.getLogger('import_manager')

class ImportManager(object):
    def __init__(self, file_path):
        self.file_path = file_path
        self.tree = None
        self._get_tree()

        #self.item_processor = ItemProcessor()

    def _get_cleaned_text(self, element):
        try:
            text = element.nodeValue
        except:
            text = u''
        if text is None:
            return u''
        return text.strip(u' ')


    def import_all(self):
        self.import_classifier()
        self.import_catalogue()
        self.import_proposals_pack()
        self.update_thumbs()
    def _get_tree(self):
        if self.tree is not None:
            return self.tree
        if not os.path.exists(self.file_path):
            message = 'File not found {}'.format(self.file_path)
            logger.error(message)
            raise OSError(message)
        try:
            DOMTree = xml.dom.minidom.parse(self.file_path)
            dom = DOMTree.documentElement
            self.tree = dom
            return dom
        except Exception as e:
            message = 'File parse error {}'.format(self.file_path)
            logger.error(message)
            raise e

    def import_classifier(self):
        logger.info('Import classifier')
        tree = self._get_tree()
        try:
            classifier_element = tree.getElementsByTagName(u'Классификатор')[0]
            if classifier_element is not None:
                self._parse_groups(classifier_element)
                self._parse_properties(classifier_element)
            else:
                logger.error("passing import_classifier...")
        except:
            logger.error("KLASSIFICATOR NOT FOUND")
            pass

    def import_catalogue(self):
        logger.info('Import catalogue')
        tree = self._get_tree()
        try:
            catalogue_element = tree.getElementsByTagName(u'Каталог')[0]
            if catalogue_element is not None:
                self._parse_products(catalogue_element)
            else:
                logger.error("passing import_catalogue...")
        except:
            logger.error("CATALOG NOT FOUND")
            pass

    def import_proposals_pack(self):
        logger.info('Import package of proposals')
        tree = self._get_tree()
        proposals_element = None
        try:
            proposals_element = tree.getElementsByTagName(u'ПакетПредложений')[0]
        except:
            try:
                proposals_element = tree.getElementsByTagName(u'ИзмененияПакетаПредложений')[0]
            except:
                 logger.error(u"PROPOSALE NOT FOUND!")
                 pass
        if proposals_element:
            self._import_tax_dict(proposals_element)
            self._import_stores_dict(proposals_element)
            self._import_proposals_products(proposals_element)  

    def _import_proposals_products(self, node):
        logger.info('Import proposals products')
        prices_dict = self.prepare_qs_data(Taxes)
        stores_dict = self.prepare_qs_data(Stores)
        parentName, childName = u"Предложения", u"Предложение"
        parent = node.getElementsByTagName(parentName)[0]
        for child in parent.getElementsByTagName(childName):
            product_cid = self.get_node_value(child, u'Ид')
            try:
                product = Product.objects.get(
                    cid = product_cid
                )
                prices = child.getElementsByTagName(u'Цены')
                if prices.length:
                    product.price.clear()
                    for z in prices[0].getElementsByTagName(u'Цена'):
                        try:
                            tax_cid = self.get_node_value(z, u'ИдТипаЦены')
                            price = self.get_node_value(z, u'ЦенаЗаЕдиницу')
                            tax_obj = prices_dict.get(tax_cid, None)
                            pt = ProductTax(
                                product = product,
                                tax = tax_obj,
                                price = price
                            )
                            pt.save()
                        except Exception, e:
                            logger.error('Import tax for %s failed! %s'%(cid, e))
                            pass
                product.quantity = child.getElementsByTagName(u'Количество')[0].childNodes[0].data
                stores = child.getElementsByTagName(u'Склад')
                try:
                    product.stores.clear()
                    for i in stores:
                        store_cid = i.getAttribute(u"ИдСклада")
                        store_quantity = i.getAttribute(u"КоличествоНаСкладе")
                        store_obj = stores_dict.get(store_cid, None)
                        if store_obj:
                            pb = ProductBalance(
                                product = product,
                                store = store_obj,
                                quantity = int(store_quantity)
                                )
                            pb.save()
                except Exception, e:
                    logger.error('Import store balance for %s failed! %s\n\n'%(cid, e))
                product.save()
            except Exception, e:
                logger.error('Fail import data for product %s! %s'%(product_cid, e))
        logger.info('Import proposals products done!\n')


    def _import_tax_dict(self, node):
        parentName, childName=u"ТипыЦен", u"ТипЦены"
        parent = node.getElementsByTagName(parentName)[0]
        for child in parent.getElementsByTagName(childName):
            cid = child.getElementsByTagName(u'Ид')[0].childNodes[0].data
            tax, created = Taxes.objects.get_or_create(cid = cid)
            try:
                tax.name = self.get_node_value(child, u'Наименование')
                tax.currency = self.get_node_value(child, u'Валюта')
                if created:
                    tax.default = True
                tax.save()
            except Exception,e:
                logger.error('Import tax error:%s, %s'%(cid, e))
        logger.info('Import taxes done!\n')


    def _import_stores_dict(self, node):
        parentName, childName=u"Склады", u"Склад"
        parent = node.getElementsByTagName(parentName)[0]
        for child in parent.getElementsByTagName(childName):
            cid = self.get_node_value(child, u'Ид')
            store, created = Stores.objects.get_or_create(cid = cid)
            try:
                store.name = self.get_node_value(child, u'Наименование')
                if created:
                    store.site_slug = "%s"%self.translit(store.name)
                    store.site_name = store.name
                store.save()
            except Exception,e:
                logger.error('Import store error:%s, %s'%(cid, e))
        logger.info('Import stores done!\n')


    def _parse_groups(self, node, parentNode=None):
        parentName, childName=u"Группы", u"Группа"
        parent = node.getElementsByTagName(parentName)[0]
        c = 0
        for child in parent.getElementsByTagName(childName):
            try:
                cid = self.get_node_value(child, u'Ид')
                name = self.get_node_value(child, u'Наименование')
                group, created = Group.objects.get_or_create(cid = cid)
                if parentNode:
                    parent, group_created = Group.objects.get_or_create(
                        cid = parentNode
                    )
                    group.parent = parent
                group.name = name
                if created:
                    list_rus = re.findall(ur'[а-яА-Я].+', name)
                    if len(list_rus):
                        group.site_name = list_rus[0].encode("utf-8")
                    group.site_slug = "%s_%d"%(self.translit(group.name), c)
                group.save()
                c = c + 1
                if child.getElementsByTagName(parentName).length > 0:
                    self._parse_groups(child, cid)
            except Exception, e:
                logger.error('Import group failed:\n%s'%e)
        logger.info('Import group done')


    def _parse_properties(self, node):
        logger.info('Import properities')
        parentName, childName = u"Свойства", u"Свойство"
        parent = node.getElementsByTagName(parentName)[0]
        for child in parent.getElementsByTagName(childName):
            cid =  self.get_node_value(child, u'Ид')
            name =  self.get_node_value(child, u'Наименование')
            type_value =  self.get_node_value(child, u'ТипЗначений')
            variants = child.getElementsByTagName(u'ВариантыЗначений')
            prop, created = Property.objects.get_or_create(cid=cid)
            prop.name = name
            prop.value_type = 2 if variants.length else 1
            if variants.length:
                for z in variants[0].getElementsByTagName(u'Справочник'):
                    var_cid =  self.get_node_value(z, u'ИдЗначения')
                    var_value =  self.get_node_value(z, u'Значение')
                    prop_variant, created = PropertyVariant.objects.get_or_create(
                        cid = var_cid
                    )
                    prop_variant.name = var_value
                    prop_variant.prop = prop
                    prop_variant.save()
            else:
                prop_variant, created = PropertyVariant.objects.get_or_create(
                    cid = cid
                )
                prop_variant.name = name
                prop_variant.prop = prop
                prop_variant.save()
            prop.save() 
        logger.info('Import properitis done!')


    def _parse_products(self, node):
        logger.info('Import products')

        dict_groups = self.prepare_qs_data(Group)
        dict_property_vars = self.prepare_qs_data(PropertyVariant)

        parentName, childName = u"Товары", u"Товар"
        parent = node.getElementsByTagName(parentName)[0]
        c = 0
        for child in parent.getElementsByTagName(childName):
            c = c + 1
            cid =  self.get_node_value(child, u'Ид')
            product, created = Product.objects.get_or_create(cid = cid)
            try:
                product.name =  self.get_node_value(child, u'Наименование')
                if created:
                    product.site_slug = "%s_%d"%(self.translit(product.name), c)
                    product.site_name =  self.get_node_value(child, u'Наименование')
                product.barcode =  self.get_node_value(child, u'Штрихкод')
                product.art =  self.get_node_value(child, u'Артикул')
                dom_group = child.getElementsByTagName(u'Группы')
                if dom_group.length:
                    try:
                        group_cid = self.get_node_value(dom_group[0], u'Ид')
                        group = dict_groups.get(group_cid, None)
                        if group:
                            product.group = dict_groups.get(group_cid, None)
                    except Exception, e:
                         logger.error('Import group for %s failed! %s'%(cid, e))
                         pass
                props = child.getElementsByTagName(u'ЗначенияСвойств')
                all_props = product.prop.all()
                if props.length:
                    for z in props[0].getElementsByTagName(u'ЗначенияСвойства'):
                        try:
                            var_cid = self.get_node_value(z, u'Значение')
                            prop_variant = dict_property_vars.get(var_cid, None)
                            if prop_variant not in all_props:
                                product.prop.add(prop_variant)
                        except Exception, e:
                            #logger.error('Import PropertyVariant for %s failed! %s'%(cid, e))
                            pass
                image_path = child.getElementsByTagName(u'Картинка')
                if image_path.length:
                    try:
                        path = image_path[0].childNodes[0].data
                        image_name = path.split("/")[-1]
                        root_file = os.path.join(settings.PRODUCTS_FOLDER_ROOT, image_name)
                        if os.path.exists(root_file):
                            add_to_product=False
                            django_file = File( open(root_file, "rb") )
                            current_images = product.images.filter(type_update = "a")
                            if len(current_images):
                                product_image = current_images[0]
                                product_image.delete()
                            else:
                                product_image = Image(
                                    title = product.name,
                                    content_object = product,
                                    type_update = "a"
                                )
                                add_to_product = True
                            product_image.image.save(image_name, django_file, save=True)
                            product_image.save()
                            if add_to_product:
                                product.images.add( product_image )
                                try: get_thumbnail(product_image, '274x274', crop='center', quality=100)
                                except:pass
                    except Exception, e:
                        logger.error('Import image %s failed!\n%s'%(cid, e))
                product.save()
            except Exception, e:
                logger.error('Import product %s failed!\n%s'%(cid, e))
        logger.info('Import products done!')


    def clear(self):
        PropertyVariant.objects.all().delete()
        Property.objects.all().delete()
        Group.objects.all().delete()
        ProductImages.objects.all().delete()
        Product.objects.all().delete()
        Taxes.objects.all().delete()
        Stores.objects.all().delete()


    def get_node_value(self, node, unicode_string):
        try:
            return node.getElementsByTagName(unicode_string)[0].childNodes[0].data
        except:
            return u""

    def prepare_qs_data(self, obj):
        res = {}
        for _o in obj.objects.all():
            res[_o.cid] = _o
        return res

    def products(self):
        return Product.objects.all()

    def props(self):
        return PropertyVariant.objects.all()

    def groups(self):
        return Group.objects.all()

    def is_update(self, node):
        try:
            return node.getAttribute(u"СодержитТолькоИзменения")
        except:
            return False


    def update_slugs(self):
        logger.info('Update site slugs started!')
        _objs = [Group, Product]
        for obj in _objs:
            c = 1
            for item in obj.objects.all():
                if not item.site_slug:
                    item.site_slug = "%s%d"%(self.translit(item.name), c)
                    item.save()
                    c = c + 1
        logger.info('Update site slugs done!')


    def translit(self, string):
        capital_letters = {u'А': u'A', u'Б': u'B', u'В': u'V', u'Г': u'G', u'Д': u'D', u'Е': u'E', u'Ё': u'E', u'Ж': u'Zh', u'З': u'Z', u'И': u'I', u'Й': u'Y', u'К': u'K', u'Л': u'L', u'М': u'M', u'Н': u'N', u'О': u'O', u'П': u'P', u'Р': u'R', u'С': u'S', u'Т': u'T', u'У': u'U', u'Ф': u'F', u'Х': u'H', u'Ц': u'Ts', u'Ч': u'Ch', u'Ш': u'Sh', u'Щ': u'Sch', u'Ъ': u'', u'Ы': u'Y', u'Ь': u'', u'Э': u'E', u'Ю': u'Yu', u'Я': u'Ya',} 
        lower_case_letters = {u'а': u'a', u'б': u'b', u'в': u'v', u'г': u'g', u'д': u'd', u'е': u'e', u'ё': u'e', u'ж': u'zh', u'з': u'z', u'и': u'i', u'й': u'y', u'к': u'k', u'л': u'l', u'м': u'm', u'н': u'n', u'о': u'o', u'п': u'p', u'р': u'r', u'с': u's', u'т': u't', u'у': u'u', u'ф': u'f', u'х': u'h', u'ц': u'ts', u'ч': u'ch', u'ш': u'sh', u'щ': u'sch', u'ъ': u'', u'ы': u'y', u'ь': u'', u'э': u'e', u'ю': u'yu', u'я': u'ya', u' ': u'_',u'/': u'_',} 
        translit_string = ""
        for index, char in enumerate(string):
            if char in lower_case_letters.keys():
                char = lower_case_letters[char]
            elif char in capital_letters.keys():
                char = capital_letters[char]
                if len(string) > index+1:
                    if string[index+1] not in lower_case_letters.keys():
                        char = char.upper()
                else:
                    char = char.upper()
            translit_string += char
        return translit_string

    def update_thumbs(self):
        for item in Product.objects.all():
            if item.image:
                try:
                    get_thumbnail(item.image.image, "274x274", crop='center', quality=100)
                    #print "%s- OK"%item.art
                except:
                    pass

"""




from claires.import_manager.manager import ImportManager
from claires.catalog.models import *
#filepath_a = "/media/hdd/Dropbox/CLAIRES_1c/2.xml"
#filepath_b = "/media/hdd/Dropbox/CLAIRES_1c/2.xml"
f=ImportManager(filepath_a)
f.update_slugs()
filepath_a="/home/claires/sites/claires/media/catalog_uploads/offers0_1.xml"
#filepath_b ="/media/sf___DESIGN/webdata/offers0_1.xml"

#f=ImportManager(filepath_a)
#f.import_classifier()
#f.import_catalogue()
#import shits
d = ImportManager(filepath_a)
d.import_proposals_pack()





f=ImportManager(filepath_a)
f.import_all()



f.clear()
f.import_all()


from claires.catalog.models import *
for p in Product.objects.all():
    #print p.get_price()
    print p.get_stores(), p.quantity



from claires.import_manager.manager import ImportManager
from claires.catalog.models import *
filepath_a="/home/greg/projects/claires/media/catalog_uploads/offers0_1.xml"

d = ImportManager(filepath_a)
#d.import_classifier()
#d.import_catalogue()
d.import_proposals_pack()



"""