# -*- coding: utf-8 -
import os
import logging
from django.http import HttpResponse, Http404
from django.views.decorators.csrf import csrf_exempt
from django.core.files.uploadedfile import SimpleUploadedFile
from .manager import ImportManager
from django.conf import settings
from claires.catalog.models import *

logger = logging.getLogger("import_manager")

IMAGE_EXT = ["jpg", "jpeg", "png"]
#@logged_in_or_basicauth()
@csrf_exempt
def front_view(request):
    return Dispatcher().dispatch(request)

def error(request, error_text):
    logger.error(error_text)
    result = '{}\n{}'.format("error", error_text)
    return HttpResponse(result)


def success(request, success_text=''):
    result = u'{}\n{}'.format("success", success_text)
    return HttpResponse(result)


def check_auth(request):
    session = request.session
    success_text = '{}\n{}'.format(settings.SESSION_COOKIE_NAME, session.session_key)
    logger.info("check_auth ok")
    return success(request, success_text)


def init(request):
    result = 'zip={}\nfile_limit={}'.format('no', str(1024*1024*256*2))
    logger.info("init ok")
    return HttpResponse(result)


def upload_file(request):
    if request.method != 'POST':
        return error(request, 'Wrong HTTP method!')
    try:
        _filename = request.GET['filename']
    except KeyError:
        logger.error("Need a filename param")
        return
    if not os.path.exists(settings.PRODUCT_UPLOAD_DIR):
        try:
            os.makedirs(settings.PRODUCT_UPLOAD_DIR)
        except OSError:
            logger.error("Cant create upload directory!")
            return
    logger.error("Upload file - %s"%_filename)    
    filename = os.path.basename(_filename)
    temp_file = SimpleUploadedFile(filename, request.read())
    filepath = os.path.join(settings.PRODUCT_UPLOAD_DIR, filename)
    try:
        _ext = filename.split(".")[-1]
        if _ext in IMAGE_EXT:
            filepath = os.path.join(settings.PRODUCTS_FOLDER_ROOT, filename)
    except Exception, e:
        logger.error("Image saving error %s"%e)    
    with open(filepath, 'wb') as f:
        for chunk in temp_file.chunks():
            f.write(chunk)
    logger.info("Uploaded file %s: ok"%_filename)
    return success(request)


def import_file(request):
    try:
        filename = request.GET['filename']
        pe = Exchange(filename=filename)
        logger.info("Try import_file - %s"%filename)
    except KeyError:
        message = "Need a filename param"
        logger.error(message)
        pe.update_status("error")
        pe.comment=message
        pe.save()
        return

    file_path = os.path.join(settings.PRODUCT_UPLOAD_DIR, filename)

    if not os.path.exists(file_path):
        message = "File doesnt exists: %s"%file_path
        logger.error(message)
        pe.update_status("error")
        pe.comment = message
        pe.save()
        return

    pe.update_status("progress")
    pe.save()
    import_manager = ImportManager(file_path)
    import_manager.import_all()

    pe.update_status("success")
    pe.save()
    return HttpResponse("success")

def export_query(request):
    pass
    """
    export_manager = ExportManager()
    export_manager.export_all()
    return HttpResponse(export_manager.get_xml(), content_type='text/xml')
    """


def export_success(request):
    pass
    """
    export_manager = ExportManager()
    Exchange.log('export', request.user)
    export_manager.flush()
    return success(request)
    """

class Dispatcher(object):

    def __init__(self):
        self.routes_map = {
            (u'catalog', u'checkauth'): check_auth,
            (u'catalog', u'init'): init,
            (u'catalog', u'file'): upload_file,
            (u'catalog', u'import'): import_file,
            (u'sale', u'file'): upload_file,
            (u'import', u'import'): import_file,
            (u'sale', u'checkauth'): check_auth,
            (u'sale', u'init'): init,
            (u'sale', u'query'): export_query,
            (u'sale', u'success'): export_success,
        }

    def dispatch(self, request):
        view_key = (request.GET.get('type'), request.GET.get('mode'))
        view = self.routes_map.get(view_key)
        if not view:
            raise Http404
        return view(request)
