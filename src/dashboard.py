# -*- coding: utf-8 -
"""
This file was generated with the customdashboard management command and
contains the class for the main dashboard.

To activate your index dashboard add the following to your settings.py::
    GRAPPELLI_INDEX_DASHBOARD = 'src.dashboard.CustomIndexDashboard'
"""

from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse

from grappelli.dashboard import modules, Dashboard
from grappelli.dashboard.utils import get_admin_site_name


class CustomIndexDashboard(Dashboard):
    """
    Custom index dashboard for www.
    """
    
    def init_with_context(self, context):
        site_name = get_admin_site_name(context)
        
        # append a group for "Administration" & "Applications"
        self.children.append(modules.Group(
            u"Админ".upper(),
            column=1,
            collapsible=True,
            children = [
                modules.AppList(
                    u"",
                    column=1,
                    collapsible=True,
                    models=('django.contrib.auth.*',),
                ),
                modules.AppList(
                    u"",
                    column=1,
                    collapsible=True,
                    models=('django.contrib.sites.*',),
                ),
                modules.LinkList(
                    _('Media Management'),
                    column=4,
                    children=[
                        {
                            'title': _('FileBrowser'),
                            'url': '/admin/filebrowser/browse/',
                            'external': False,
                        },
                    ]
                )

            ]
        ))
        self.children.append(modules.Group(
            u"Сайт".upper(),
            column=2,
            collapsible=True,
            children = [
                modules.AppList(
                    u'Каталог',
                    column=1,
                    css_classes=('collapse closed',),
                    models=('claires.catalog.*',),
                ),
                modules.AppList(
                    u'Страницы',
                    column=2,
                    css_classes=('collapse closed',),
                    models=('claires.pages.*',),
                ),
                modules.AppList(
                    u'Баннеры',
                    column=2,
                    css_classes=('collapse closed',),
                    models=('claires.banners.*',),
                ),
                modules.AppList(
                    u'',
                    column=2,
                    css_classes=('collapse closed',),
                    models=('claires.locations.*',),
                ),
                modules.AppList(
                    u'',
                    column=2,
                    css_classes=('collapse closed',),
                    models=('claires.vacancies.*',),
                ),
                modules.AppList(
                    u'',
                    column=2,
                    css_classes=('collapse closed',),
                    models=('claires.user.*',),
                )
            ]
        ))

        self.children.append(modules.Group(
            u"Контент",
            column=2,
            collapsible=False,
            children = [

            ]
        ))
   
        # append a recent actions module
        self.children.append(modules.RecentActions(
            _('Recent Actions'),
            limit=20,
            collapsible=False,
            column=3,
        ))


