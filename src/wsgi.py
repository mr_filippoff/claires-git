#!/mnt/projects/Valtera/venv/bin/python
import sys,os,site

site.addsitedir('/usr/bin/python2.7/site-packages')

os.environ['DJANGO_SETTINGS_MODULE'] = 'claires.settings.local'

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()